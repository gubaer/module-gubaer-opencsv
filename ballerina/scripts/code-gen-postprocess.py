import re
import shutil

PARSE_LINE_REPLACEMENT = """
    # The function that maps to the `parseLine` method of `com.opencsv.AbstractCSVParser`.
    #
    # + arg0 - The `string` value required to map with the Java method parameter.
    # + return - The `string?[]` or the `javaio:IOException` value returning from the Java mapping.
    public function parseLine(string arg0) returns string?[]|javaio:IOException|error {
        handle|error externalObj = com_opencsv_CSVParser_parseLine(self.jObj, java:fromString(arg0));
        if externalObj is error {
            return error javaio:IOException(javaio:IOEXCEPTION, externalObj, message = externalObj.message());
        } 
        any[] values = check jarrays:fromHandle(externalObj, "handle");
        return values.map(v => java:toString(<handle>v));
    }
"""

CSVREADER_READNEXT = """
    # The function that maps to the `readNext` method of `com.opencsv.CSVReader`.
    #
    # + return - The `string[]` or the `javaio:IOException` value returning from the Java mapping.
    public function readNext() returns string[]?|javaio:IOException|error {
        handle|error externalObj = com_opencsv_CSVReader_readNext(self.jObj);
        if externalObj is error {
            return error javaio:IOException(javaio:IOEXCEPTION, externalObj, message = externalObj.message());
        }
        if java:isNull(externalObj) {
            return null;
        }
        return <string[]>check jarrays:fromHandle(externalObj, "string");
    }
"""

CSVREADER_READNEXTSILENTLY = """
    # The function that maps to the `readNextSilently` method of `com.opencsv.CSVReader`.
    #
    # + return - The `string[]` or the `javaio:IOException` value returning from the Java mapping.
    public function readNextSilently() returns string[]?|javaio:IOException|error {
        handle|error externalObj = com_opencsv_CSVReader_readNextSilently(self.jObj);
        if externalObj is error {
            return error javaio:IOException(javaio:IOEXCEPTION, externalObj, message = externalObj.message());
        }
        if java:isNull(externalObj) {
            return null;
        }
        return <string[]>check jarrays:fromHandle(externalObj, "string");
    }
"""

def find_method_borders(source, start_pattern, end_pattern):
    start = 0
    source_len = len(source)
    while True:
        if start >= source_len:
            return (None, None)
        if start_pattern.match(source[start]):
            break
        else:
            start += 1
    end = start + 1
    while True:
        if start >= source_len:
            return (start, None)
        if end_pattern.match(source[end]):
            break
        else:
            end += 1
    return (start, end)


def expand_to_start_of_method_comment(source, borders):
    doc_comment_line_pattern = re.compile('^\s+#')

    if borders[0] is None or borders[0] < 0 or borders[0] >= len(source):
        raise ValueError(f"unexpected borders argument, got {borders}")
    start = borders[0] - 1
    # move to first non-empty line before method start
    while True:
        if start < 0:
            return (None, borders[1])
        if source[start].strip() == "":
            start -= 1
        else:
            break
    # move to first line of doc comment for the method
    while True:
        if start < 0:
            return (None, borders[1])
        if doc_comment_line_pattern.match(source[start]):
            start -= 1
        else:
            return (start + 1, borders[1])


def process_source(in_file, function_name, new_function_body):
    out_file = f"{in_file}.tmp"
    start_pattern = re.compile('\s+public\s+function\s+' + function_name + '\W')
    end_pattern = re.compile('^    }')
    with open(in_file, "r") as source:
        with open(out_file, "w") as target:
            source = source.read().splitlines()
            borders = find_method_borders(source, start_pattern, end_pattern)
            if borders[0] is None:
                print(f"FATAL: start of method '{function_name}' not found")
                return
            elif borders[1] is None:
                print(f"Fatal: end of method '{function_name}' not found")
                return
            with_doc_comments_borders = expand_to_start_of_method_comment(source, borders)
            if with_doc_comments_borders[1] is None:
                print(f"Warning: no doc comments found for method '{function_name}'")
            else:
                borders = with_doc_comments_borders
            rewritten_source = source[:borders[0]]
            rewritten_source += new_function_body.splitlines()
            rewritten_source += source[borders[1] + 1:]
            print("\n".join(rewritten_source), file=target)
    shutil.move(out_file, in_file)

def main():
    process_source("modules/com.opencsv/AbstractCSVParser.bal", "parseLine", PARSE_LINE_REPLACEMENT)
    process_source("modules/com.opencsv/CSVParser.bal", "parseLine", PARSE_LINE_REPLACEMENT)
    process_source("modules/com.opencsv/CSVReader.bal", "readNext", CSVREADER_READNEXT)
    process_source("modules/com.opencsv/CSVReader.bal", "readNextSilently", CSVREADER_READNEXTSILENTLY)

if __name__ == '__main__':
    main()
