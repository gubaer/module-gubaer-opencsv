import ballerina/test;
import csvplus.com.opencsv as csv;
import csvplus.com.opencsv.enums as enums;


@test:Config{}
public function testParserBuilder_01() returns error? {
    var builder = csv:newCSVParserBuilder1();
    var _ = builder.build();
}

@test:Config{}
public function testParserBuilder_02() returns error? {
    csv:CSVParserBuilder builder = check createCSVParserBuilder();
    builder = builder
        .withSeparator(";".toCodePointInt())
        .withQuoteChar("'".toCodePointInt())
        .withEscapeChar("\\".toCodePointInt());

    csv:CSVParser parser = builder.build();

    final string line = "a;'b';'c\\''";
    var result = check parser.parseLine(line);

    test:assertEquals(result[0], "a");
    test:assertEquals(result[1], "b");
    test:assertEquals(result[2], "c'");
}

@test:Config{}
public function testParserBuilder_03() returns error? {
    csv:CSVParserBuilder builder = check createCSVParserBuilder();

    var indicator = enums:CSVReaderNullFieldIndicator_getBOTH();

    builder = builder
        .withQuoteChar("\"".toCodePointInt())
        .withFieldAsNull(indicator);
    
    csv:CSVParser parser = builder.build();

    final string line = string`a,,c,""`;
    var result = check parser.parseLine(line);

    test:assertEquals(result[0], "a");
    test:assertEquals(result[1], null);
    test:assertEquals(result[2], "c");
    test:assertEquals(result[3], null);
}

@test:Config{}
public function testParserBuilder_04() returns error? {
    csv:CSVParserBuilder builder = check createCSVParserBuilder();

    builder = builder
        .withQuoteChar("\"".toCodePointInt())
        .withIgnoreLeadingWhiteSpace(true);
    
    csv:CSVParser parser = builder.build();

    string line = string`a,   "b",c`;
    var result = check parser.parseLine(line);

    test:assertEquals(result[0], "a");
    test:assertEquals(result[1], "b");
    test:assertEquals(result[2], "c");


    builder = check createCSVParserBuilder();
    builder = builder
        .withQuoteChar("\"".toCodePointInt())
        .withIgnoreLeadingWhiteSpace(false);
    
    parser = builder.build();

    line = string`a,   "b",c`;
    result = check parser.parseLine(line);

    test:assertEquals(result[0], "a");
    test:assertEquals(result[1], "   \"b");
    test:assertEquals(result[2], "c");
}

@test:Config{}
public function testParserBuilder_05() returns error? {
    csv:CSVParserBuilder builder = check createCSVParserBuilder();

    builder = builder
        .withQuoteChar("\"".toCodePointInt())
        .withIgnoreQuotations(true);
    
    csv:CSVParser parser = builder.build();

    final string line = string`"a","b",  "c"`;
    var result = check parser.parseLine(line);

    test:assertEquals(result[0], "a");
    test:assertEquals(result[1], "b");
    test:assertEquals(result[2], "c");
}
