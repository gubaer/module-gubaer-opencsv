import csvplus.java.io as javaio;
import ballerina/test;

@test:Config{}
public function canCreateAFileReader() returns error? {
    javaio:File f = javaio:newFile2("tests/test-01.txt");
    javaio:FileReader fr = check javaio:newFileReader1(f);
    javaio:BufferedReader reader = javaio:newBufferedReader1(fr);

    string? content = check reader.readLine();
    test:assertTrue(content != null);
    test:assertEquals("test", content);
}


@test:Config{}
public function canCreateAFileReaderWithoutAFile() returns error? {
    var fr = check javaio:newFileReader4("tests/test-01.txt");
    var reader = javaio:newBufferedReader1(fr);

    string? content = check reader.readLine();
    test:assertTrue(content != null);
    test:assertEquals("test", content);
}



