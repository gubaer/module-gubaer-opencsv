import ballerina/jballerina.java;
import csvplus.java.util as javautil;
import csvplus.java.lang as javalang;
import csvplus.java.io as javaio;
import csvplus.com.opencsv.enums as comopencsvenums;
import csvplus.com.opencsv.validators as comopencsvvalidators;
import csvplus.com.opencsv.processor as comopencsvprocessor;

# Ballerina class mapping for the Java `com.opencsv.CSVReaderBuilder` class.
@java:Binding {'class: "com.opencsv.CSVReaderBuilder"}
public distinct class CSVReaderBuilder {

    *java:JObject;
    *javalang:Object;

    # The `handle` field that stores the reference to the `com.opencsv.CSVReaderBuilder` object.
    public handle jObj;

    # The init function of the Ballerina class mapping the `com.opencsv.CSVReaderBuilder` Java class.
    #
    # + obj - The `handle` value containing the Java reference of the object.
    public function init(handle obj) {
        self.jObj = obj;
    }

    # The function to retrieve the string representation of the Ballerina class mapping the `com.opencsv.CSVReaderBuilder` Java class.
    #
    # + return - The `string` form of the Java object instance.
    public function toString() returns string {
        return java:toString(self.jObj) ?: "null";
    }
    # The function that maps to the `build` method of `com.opencsv.CSVReaderBuilder`.
    #
    # + return - The `CSVReader` value returning from the Java mapping.
    public function build() returns CSVReader {
        handle externalObj = com_opencsv_CSVReaderBuilder_build(self.jObj);
        CSVReader newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `equals` method of `com.opencsv.CSVReaderBuilder`.
    #
    # + arg0 - The `javalang:Object` value required to map with the Java method parameter.
    # + return - The `boolean` value returning from the Java mapping.
    public function 'equals(javalang:Object arg0) returns boolean {
        return com_opencsv_CSVReaderBuilder_equals(self.jObj, arg0.jObj);
    }

    # The function that maps to the `getClass` method of `com.opencsv.CSVReaderBuilder`.
    #
    # + return - The `javalang:Class` value returning from the Java mapping.
    public function getClass() returns javalang:Class {
        handle externalObj = com_opencsv_CSVReaderBuilder_getClass(self.jObj);
        javalang:Class newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getErrorLocale` method of `com.opencsv.CSVReaderBuilder`.
    #
    # + return - The `javautil:Locale` value returning from the Java mapping.
    public function getErrorLocale() returns javautil:Locale {
        handle externalObj = com_opencsv_CSVReaderBuilder_getErrorLocale(self.jObj);
        javautil:Locale newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getLineValidatorAggregator` method of `com.opencsv.CSVReaderBuilder`.
    #
    # + return - The `comopencsvvalidators:LineValidatorAggregator` value returning from the Java mapping.
    public function getLineValidatorAggregator() returns comopencsvvalidators:LineValidatorAggregator {
        handle externalObj = com_opencsv_CSVReaderBuilder_getLineValidatorAggregator(self.jObj);
        comopencsvvalidators:LineValidatorAggregator newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getRowValidatorAggregator` method of `com.opencsv.CSVReaderBuilder`.
    #
    # + return - The `comopencsvvalidators:RowValidatorAggregator` value returning from the Java mapping.
    public function getRowValidatorAggregator() returns comopencsvvalidators:RowValidatorAggregator {
        handle externalObj = com_opencsv_CSVReaderBuilder_getRowValidatorAggregator(self.jObj);
        comopencsvvalidators:RowValidatorAggregator newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `hashCode` method of `com.opencsv.CSVReaderBuilder`.
    #
    # + return - The `int` value returning from the Java mapping.
    public function hashCode() returns int {
        return com_opencsv_CSVReaderBuilder_hashCode(self.jObj);
    }

    # The function that maps to the `isVerifyReader` method of `com.opencsv.CSVReaderBuilder`.
    #
    # + return - The `boolean` value returning from the Java mapping.
    public function isVerifyReader() returns boolean {
        return com_opencsv_CSVReaderBuilder_isVerifyReader(self.jObj);
    }

    # The function that maps to the `notify` method of `com.opencsv.CSVReaderBuilder`.
    public function notify() {
        com_opencsv_CSVReaderBuilder_notify(self.jObj);
    }

    # The function that maps to the `notifyAll` method of `com.opencsv.CSVReaderBuilder`.
    public function notifyAll() {
        com_opencsv_CSVReaderBuilder_notifyAll(self.jObj);
    }

    # The function that maps to the `wait` method of `com.opencsv.CSVReaderBuilder`.
    #
    # + return - The `javalang:InterruptedException` value returning from the Java mapping.
    public function 'wait() returns javalang:InterruptedException? {
        error|() externalObj = com_opencsv_CSVReaderBuilder_wait(self.jObj);
        if (externalObj is error) {
            javalang:InterruptedException e = error javalang:InterruptedException(javalang:INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `wait` method of `com.opencsv.CSVReaderBuilder`.
    #
    # + arg0 - The `int` value required to map with the Java method parameter.
    # + return - The `javalang:InterruptedException` value returning from the Java mapping.
    public function wait2(int arg0) returns javalang:InterruptedException? {
        error|() externalObj = com_opencsv_CSVReaderBuilder_wait2(self.jObj, arg0);
        if (externalObj is error) {
            javalang:InterruptedException e = error javalang:InterruptedException(javalang:INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `wait` method of `com.opencsv.CSVReaderBuilder`.
    #
    # + arg0 - The `int` value required to map with the Java method parameter.
    # + arg1 - The `int` value required to map with the Java method parameter.
    # + return - The `javalang:InterruptedException` value returning from the Java mapping.
    public function wait3(int arg0, int arg1) returns javalang:InterruptedException? {
        error|() externalObj = com_opencsv_CSVReaderBuilder_wait3(self.jObj, arg0, arg1);
        if (externalObj is error) {
            javalang:InterruptedException e = error javalang:InterruptedException(javalang:INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `withCSVParser` method of `com.opencsv.CSVReaderBuilder`.
    #
    # + arg0 - The `ICSVParser` value required to map with the Java method parameter.
    # + return - The `CSVReaderBuilder` value returning from the Java mapping.
    public function withCSVParser(ICSVParser arg0) returns CSVReaderBuilder {
        handle externalObj = com_opencsv_CSVReaderBuilder_withCSVParser(self.jObj, arg0.jObj);
        CSVReaderBuilder newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `withErrorLocale` method of `com.opencsv.CSVReaderBuilder`.
    #
    # + arg0 - The `javautil:Locale` value required to map with the Java method parameter.
    # + return - The `CSVReaderBuilder` value returning from the Java mapping.
    public function withErrorLocale(javautil:Locale arg0) returns CSVReaderBuilder {
        handle externalObj = com_opencsv_CSVReaderBuilder_withErrorLocale(self.jObj, arg0.jObj);
        CSVReaderBuilder newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `withFieldAsNull` method of `com.opencsv.CSVReaderBuilder`.
    #
    # + arg0 - The `comopencsvenums:CSVReaderNullFieldIndicator` value required to map with the Java method parameter.
    # + return - The `CSVReaderBuilder` value returning from the Java mapping.
    public function withFieldAsNull(comopencsvenums:CSVReaderNullFieldIndicator arg0) returns CSVReaderBuilder {
        handle externalObj = com_opencsv_CSVReaderBuilder_withFieldAsNull(self.jObj, arg0.jObj);
        CSVReaderBuilder newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `withKeepCarriageReturn` method of `com.opencsv.CSVReaderBuilder`.
    #
    # + arg0 - The `boolean` value required to map with the Java method parameter.
    # + return - The `CSVReaderBuilder` value returning from the Java mapping.
    public function withKeepCarriageReturn(boolean arg0) returns CSVReaderBuilder {
        handle externalObj = com_opencsv_CSVReaderBuilder_withKeepCarriageReturn(self.jObj, arg0);
        CSVReaderBuilder newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `withLineValidator` method of `com.opencsv.CSVReaderBuilder`.
    #
    # + arg0 - The `comopencsvvalidators:LineValidator` value required to map with the Java method parameter.
    # + return - The `CSVReaderBuilder` value returning from the Java mapping.
    public function withLineValidator(comopencsvvalidators:LineValidator arg0) returns CSVReaderBuilder {
        handle externalObj = com_opencsv_CSVReaderBuilder_withLineValidator(self.jObj, arg0.jObj);
        CSVReaderBuilder newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `withMultilineLimit` method of `com.opencsv.CSVReaderBuilder`.
    #
    # + arg0 - The `int` value required to map with the Java method parameter.
    # + return - The `CSVReaderBuilder` value returning from the Java mapping.
    public function withMultilineLimit(int arg0) returns CSVReaderBuilder {
        handle externalObj = com_opencsv_CSVReaderBuilder_withMultilineLimit(self.jObj, arg0);
        CSVReaderBuilder newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `withRowProcessor` method of `com.opencsv.CSVReaderBuilder`.
    #
    # + arg0 - The `comopencsvprocessor:RowProcessor` value required to map with the Java method parameter.
    # + return - The `CSVReaderBuilder` value returning from the Java mapping.
    public function withRowProcessor(comopencsvprocessor:RowProcessor arg0) returns CSVReaderBuilder {
        handle externalObj = com_opencsv_CSVReaderBuilder_withRowProcessor(self.jObj, arg0.jObj);
        CSVReaderBuilder newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `withRowValidator` method of `com.opencsv.CSVReaderBuilder`.
    #
    # + arg0 - The `comopencsvvalidators:RowValidator` value required to map with the Java method parameter.
    # + return - The `CSVReaderBuilder` value returning from the Java mapping.
    public function withRowValidator(comopencsvvalidators:RowValidator arg0) returns CSVReaderBuilder {
        handle externalObj = com_opencsv_CSVReaderBuilder_withRowValidator(self.jObj, arg0.jObj);
        CSVReaderBuilder newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `withSkipLines` method of `com.opencsv.CSVReaderBuilder`.
    #
    # + arg0 - The `int` value required to map with the Java method parameter.
    # + return - The `CSVReaderBuilder` value returning from the Java mapping.
    public function withSkipLines(int arg0) returns CSVReaderBuilder {
        handle externalObj = com_opencsv_CSVReaderBuilder_withSkipLines(self.jObj, arg0);
        CSVReaderBuilder newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `withVerifyReader` method of `com.opencsv.CSVReaderBuilder`.
    #
    # + arg0 - The `boolean` value required to map with the Java method parameter.
    # + return - The `CSVReaderBuilder` value returning from the Java mapping.
    public function withVerifyReader(boolean arg0) returns CSVReaderBuilder {
        handle externalObj = com_opencsv_CSVReaderBuilder_withVerifyReader(self.jObj, arg0);
        CSVReaderBuilder newObj = new (externalObj);
        return newObj;
    }

}

# The constructor function to generate an object of `com.opencsv.CSVReaderBuilder`.
#
# + arg0 - The `javaio:Reader` value required to map with the Java constructor parameter.
# + return - The new `CSVReaderBuilder` class generated.
public function newCSVReaderBuilder1(javaio:Reader arg0) returns CSVReaderBuilder {
    handle externalObj = com_opencsv_CSVReaderBuilder_newCSVReaderBuilder1(arg0.jObj);
    CSVReaderBuilder newObj = new (externalObj);
    return newObj;
}

function com_opencsv_CSVReaderBuilder_build(handle receiver) returns handle = @java:Method {
    name: "build",
    'class: "com.opencsv.CSVReaderBuilder",
    paramTypes: []
} external;

function com_opencsv_CSVReaderBuilder_equals(handle receiver, handle arg0) returns boolean = @java:Method {
    name: "equals",
    'class: "com.opencsv.CSVReaderBuilder",
    paramTypes: ["java.lang.Object"]
} external;

function com_opencsv_CSVReaderBuilder_getClass(handle receiver) returns handle = @java:Method {
    name: "getClass",
    'class: "com.opencsv.CSVReaderBuilder",
    paramTypes: []
} external;

function com_opencsv_CSVReaderBuilder_getErrorLocale(handle receiver) returns handle = @java:Method {
    name: "getErrorLocale",
    'class: "com.opencsv.CSVReaderBuilder",
    paramTypes: []
} external;

function com_opencsv_CSVReaderBuilder_getLineValidatorAggregator(handle receiver) returns handle = @java:Method {
    name: "getLineValidatorAggregator",
    'class: "com.opencsv.CSVReaderBuilder",
    paramTypes: []
} external;

function com_opencsv_CSVReaderBuilder_getRowValidatorAggregator(handle receiver) returns handle = @java:Method {
    name: "getRowValidatorAggregator",
    'class: "com.opencsv.CSVReaderBuilder",
    paramTypes: []
} external;

function com_opencsv_CSVReaderBuilder_hashCode(handle receiver) returns int = @java:Method {
    name: "hashCode",
    'class: "com.opencsv.CSVReaderBuilder",
    paramTypes: []
} external;

function com_opencsv_CSVReaderBuilder_isVerifyReader(handle receiver) returns boolean = @java:Method {
    name: "isVerifyReader",
    'class: "com.opencsv.CSVReaderBuilder",
    paramTypes: []
} external;

function com_opencsv_CSVReaderBuilder_notify(handle receiver) = @java:Method {
    name: "notify",
    'class: "com.opencsv.CSVReaderBuilder",
    paramTypes: []
} external;

function com_opencsv_CSVReaderBuilder_notifyAll(handle receiver) = @java:Method {
    name: "notifyAll",
    'class: "com.opencsv.CSVReaderBuilder",
    paramTypes: []
} external;

function com_opencsv_CSVReaderBuilder_wait(handle receiver) returns error? = @java:Method {
    name: "wait",
    'class: "com.opencsv.CSVReaderBuilder",
    paramTypes: []
} external;

function com_opencsv_CSVReaderBuilder_wait2(handle receiver, int arg0) returns error? = @java:Method {
    name: "wait",
    'class: "com.opencsv.CSVReaderBuilder",
    paramTypes: ["long"]
} external;

function com_opencsv_CSVReaderBuilder_wait3(handle receiver, int arg0, int arg1) returns error? = @java:Method {
    name: "wait",
    'class: "com.opencsv.CSVReaderBuilder",
    paramTypes: ["long", "int"]
} external;

function com_opencsv_CSVReaderBuilder_withCSVParser(handle receiver, handle arg0) returns handle = @java:Method {
    name: "withCSVParser",
    'class: "com.opencsv.CSVReaderBuilder",
    paramTypes: ["com.opencsv.ICSVParser"]
} external;

function com_opencsv_CSVReaderBuilder_withErrorLocale(handle receiver, handle arg0) returns handle = @java:Method {
    name: "withErrorLocale",
    'class: "com.opencsv.CSVReaderBuilder",
    paramTypes: ["java.util.Locale"]
} external;

function com_opencsv_CSVReaderBuilder_withFieldAsNull(handle receiver, handle arg0) returns handle = @java:Method {
    name: "withFieldAsNull",
    'class: "com.opencsv.CSVReaderBuilder",
    paramTypes: ["com.opencsv.enums.CSVReaderNullFieldIndicator"]
} external;

function com_opencsv_CSVReaderBuilder_withKeepCarriageReturn(handle receiver, boolean arg0) returns handle = @java:Method {
    name: "withKeepCarriageReturn",
    'class: "com.opencsv.CSVReaderBuilder",
    paramTypes: ["boolean"]
} external;

function com_opencsv_CSVReaderBuilder_withLineValidator(handle receiver, handle arg0) returns handle = @java:Method {
    name: "withLineValidator",
    'class: "com.opencsv.CSVReaderBuilder",
    paramTypes: ["com.opencsv.validators.LineValidator"]
} external;

function com_opencsv_CSVReaderBuilder_withMultilineLimit(handle receiver, int arg0) returns handle = @java:Method {
    name: "withMultilineLimit",
    'class: "com.opencsv.CSVReaderBuilder",
    paramTypes: ["int"]
} external;

function com_opencsv_CSVReaderBuilder_withRowProcessor(handle receiver, handle arg0) returns handle = @java:Method {
    name: "withRowProcessor",
    'class: "com.opencsv.CSVReaderBuilder",
    paramTypes: ["com.opencsv.processor.RowProcessor"]
} external;

function com_opencsv_CSVReaderBuilder_withRowValidator(handle receiver, handle arg0) returns handle = @java:Method {
    name: "withRowValidator",
    'class: "com.opencsv.CSVReaderBuilder",
    paramTypes: ["com.opencsv.validators.RowValidator"]
} external;

function com_opencsv_CSVReaderBuilder_withSkipLines(handle receiver, int arg0) returns handle = @java:Method {
    name: "withSkipLines",
    'class: "com.opencsv.CSVReaderBuilder",
    paramTypes: ["int"]
} external;

function com_opencsv_CSVReaderBuilder_withVerifyReader(handle receiver, boolean arg0) returns handle = @java:Method {
    name: "withVerifyReader",
    'class: "com.opencsv.CSVReaderBuilder",
    paramTypes: ["boolean"]
} external;

function com_opencsv_CSVReaderBuilder_newCSVReaderBuilder1(handle arg0) returns handle = @java:Constructor {
    'class: "com.opencsv.CSVReaderBuilder",
    paramTypes: ["java.io.Reader"]
} external;

