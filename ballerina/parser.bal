import csvplus.com.opencsv;

public function createCSVParser() returns opencsv:CSVParser |error {
    return opencsv:newCSVParser1();
}

public function createCSVParserBuilder() returns opencsv:CSVParserBuilder | error {
    return opencsv:newCSVParserBuilder1();
}