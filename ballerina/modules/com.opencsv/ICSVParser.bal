import ballerina/jballerina.java;
import ballerina/jballerina.java.arrays as jarrays;
import csvplus.java.util as javautil;
import csvplus.com.opencsv.enums as comopencsvenums;
import csvplus.java.io as javaio;

# Ballerina class mapping for the Java `com.opencsv.ICSVParser` interface.
@java:Binding {'class: "com.opencsv.ICSVParser"}
public distinct class ICSVParser {

    *java:JObject;

    # The `handle` field that stores the reference to the `com.opencsv.ICSVParser` object.
    public handle jObj;

    # The init function of the Ballerina class mapping the `com.opencsv.ICSVParser` Java interface.
    #
    # + obj - The `handle` value containing the Java reference of the object.
    public function init(handle obj) {
        self.jObj = obj;
    }

    # The function to retrieve the string representation of the Ballerina class mapping the `com.opencsv.ICSVParser` Java interface.
    #
    # + return - The `string` form of the Java object instance.
    public function toString() returns string {
        return java:toString(self.jObj) ?: "null";
    }
    # The function that maps to the `getPendingText` method of `com.opencsv.ICSVParser`.
    #
    # + return - The `string` value returning from the Java mapping.
    public function getPendingText() returns string? {
        return java:toString(com_opencsv_ICSVParser_getPendingText(self.jObj));
    }

    # The function that maps to the `getQuotechar` method of `com.opencsv.ICSVParser`.
    #
    # + return - The `int` value returning from the Java mapping.
    public function getQuotechar() returns int {
        return com_opencsv_ICSVParser_getQuotechar(self.jObj);
    }

    # The function that maps to the `getSeparator` method of `com.opencsv.ICSVParser`.
    #
    # + return - The `int` value returning from the Java mapping.
    public function getSeparator() returns int {
        return com_opencsv_ICSVParser_getSeparator(self.jObj);
    }

    # The function that maps to the `isPending` method of `com.opencsv.ICSVParser`.
    #
    # + return - The `boolean` value returning from the Java mapping.
    public function isPending() returns boolean {
        return com_opencsv_ICSVParser_isPending(self.jObj);
    }

    # The function that maps to the `nullFieldIndicator` method of `com.opencsv.ICSVParser`.
    #
    # + return - The `comopencsvenums:CSVReaderNullFieldIndicator` value returning from the Java mapping.
    public function nullFieldIndicator() returns comopencsvenums:CSVReaderNullFieldIndicator {
        handle externalObj = com_opencsv_ICSVParser_nullFieldIndicator(self.jObj);
        comopencsvenums:CSVReaderNullFieldIndicator newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `parseLine` method of `com.opencsv.ICSVParser`.
    #
    # + arg0 - The `string` value required to map with the Java method parameter.
    # + return - The `string[]` or the `javaio:IOException` value returning from the Java mapping.
    public function parseLine(string arg0) returns string[]|javaio:IOException|error {
        handle|error externalObj = com_opencsv_ICSVParser_parseLine(self.jObj, java:fromString(arg0));
        if (externalObj is error) {
            javaio:IOException e = error javaio:IOException(javaio:IOEXCEPTION, externalObj, message = externalObj.message());
            return e;
        } else {
            return <string[]>check jarrays:fromHandle(externalObj, "string");
        }
    }

    # The function that maps to the `parseLineMulti` method of `com.opencsv.ICSVParser`.
    #
    # + arg0 - The `string` value required to map with the Java method parameter.
    # + return - The `string[]` or the `javaio:IOException` value returning from the Java mapping.
    public function parseLineMulti(string arg0) returns string[]|javaio:IOException|error {
        handle|error externalObj = com_opencsv_ICSVParser_parseLineMulti(self.jObj, java:fromString(arg0));
        if (externalObj is error) {
            javaio:IOException e = error javaio:IOException(javaio:IOEXCEPTION, externalObj, message = externalObj.message());
            return e;
        } else {
            return <string[]>check jarrays:fromHandle(externalObj, "string");
        }
    }

    # The function that maps to the `parseToLine` method of `com.opencsv.ICSVParser`.
    #
    # + arg0 - The `string[]` value required to map with the Java method parameter.
    # + arg1 - The `boolean` value required to map with the Java method parameter.
    # + return - The `string` value returning from the Java mapping.
    public function parseToLine(string[] arg0, boolean arg1) returns string?|error {
        return java:toString(com_opencsv_ICSVParser_parseToLine(self.jObj, check jarrays:toHandle(arg0, "java.lang.String"), arg1));
    }

    # The function that maps to the `setErrorLocale` method of `com.opencsv.ICSVParser`.
    #
    # + arg0 - The `javautil:Locale` value required to map with the Java method parameter.
    public function setErrorLocale(javautil:Locale arg0) {
        com_opencsv_ICSVParser_setErrorLocale(self.jObj, arg0.jObj);
    }

}

# The function that retrieves the value of the public field `DEFAULT_SEPARATOR`.
#
# + return - The `int` value of the field.
public function ICSVParser_getDEFAULT_SEPARATOR() returns int {
    return com_opencsv_ICSVParser_getDEFAULT_SEPARATOR();
}

# The function that retrieves the value of the public field `INITIAL_READ_SIZE`.
#
# + return - The `int` value of the field.
public function ICSVParser_getINITIAL_READ_SIZE() returns int {
    return com_opencsv_ICSVParser_getINITIAL_READ_SIZE();
}

# The function that retrieves the value of the public field `READ_BUFFER_SIZE`.
#
# + return - The `int` value of the field.
public function ICSVParser_getREAD_BUFFER_SIZE() returns int {
    return com_opencsv_ICSVParser_getREAD_BUFFER_SIZE();
}

# The function that retrieves the value of the public field `DEFAULT_QUOTE_CHARACTER`.
#
# + return - The `int` value of the field.
public function ICSVParser_getDEFAULT_QUOTE_CHARACTER() returns int {
    return com_opencsv_ICSVParser_getDEFAULT_QUOTE_CHARACTER();
}

# The function that retrieves the value of the public field `DEFAULT_ESCAPE_CHARACTER`.
#
# + return - The `int` value of the field.
public function ICSVParser_getDEFAULT_ESCAPE_CHARACTER() returns int {
    return com_opencsv_ICSVParser_getDEFAULT_ESCAPE_CHARACTER();
}

# The function that retrieves the value of the public field `DEFAULT_STRICT_QUOTES`.
#
# + return - The `boolean` value of the field.
public function ICSVParser_getDEFAULT_STRICT_QUOTES() returns boolean {
    return com_opencsv_ICSVParser_getDEFAULT_STRICT_QUOTES();
}

# The function that retrieves the value of the public field `DEFAULT_IGNORE_LEADING_WHITESPACE`.
#
# + return - The `boolean` value of the field.
public function ICSVParser_getDEFAULT_IGNORE_LEADING_WHITESPACE() returns boolean {
    return com_opencsv_ICSVParser_getDEFAULT_IGNORE_LEADING_WHITESPACE();
}

# The function that retrieves the value of the public field `DEFAULT_IGNORE_QUOTATIONS`.
#
# + return - The `boolean` value of the field.
public function ICSVParser_getDEFAULT_IGNORE_QUOTATIONS() returns boolean {
    return com_opencsv_ICSVParser_getDEFAULT_IGNORE_QUOTATIONS();
}

# The function that retrieves the value of the public field `NULL_CHARACTER`.
#
# + return - The `int` value of the field.
public function ICSVParser_getNULL_CHARACTER() returns int {
    return com_opencsv_ICSVParser_getNULL_CHARACTER();
}

# The function that retrieves the value of the public field `DEFAULT_NULL_FIELD_INDICATOR`.
#
# + return - The `CSVReaderNullFieldIndicator` value of the field.
public function ICSVParser_getDEFAULT_NULL_FIELD_INDICATOR() returns comopencsvenums:CSVReaderNullFieldIndicator {
    handle externalObj = com_opencsv_ICSVParser_getDEFAULT_NULL_FIELD_INDICATOR();
    comopencsvenums:CSVReaderNullFieldIndicator newObj = new (externalObj);
    return newObj;
}

# The function that retrieves the value of the public field `DEFAULT_BUNDLE_NAME`.
#
# + return - The `string` value of the field.
public function ICSVParser_getDEFAULT_BUNDLE_NAME() returns string? {
    return java:toString(com_opencsv_ICSVParser_getDEFAULT_BUNDLE_NAME());
}

# The function that retrieves the value of the public field `MAX_SIZE_FOR_EMPTY_FIELD`.
#
# + return - The `int` value of the field.
public function ICSVParser_getMAX_SIZE_FOR_EMPTY_FIELD() returns int {
    return com_opencsv_ICSVParser_getMAX_SIZE_FOR_EMPTY_FIELD();
}

# The function that retrieves the value of the public field `NEWLINE`.
#
# + return - The `string` value of the field.
public function ICSVParser_getNEWLINE() returns string? {
    return java:toString(com_opencsv_ICSVParser_getNEWLINE());
}

function com_opencsv_ICSVParser_getPendingText(handle receiver) returns handle = @java:Method {
    name: "getPendingText",
    'class: "com.opencsv.ICSVParser",
    paramTypes: []
} external;

function com_opencsv_ICSVParser_getQuotechar(handle receiver) returns int = @java:Method {
    name: "getQuotechar",
    'class: "com.opencsv.ICSVParser",
    paramTypes: []
} external;

function com_opencsv_ICSVParser_getSeparator(handle receiver) returns int = @java:Method {
    name: "getSeparator",
    'class: "com.opencsv.ICSVParser",
    paramTypes: []
} external;

function com_opencsv_ICSVParser_isPending(handle receiver) returns boolean = @java:Method {
    name: "isPending",
    'class: "com.opencsv.ICSVParser",
    paramTypes: []
} external;

function com_opencsv_ICSVParser_nullFieldIndicator(handle receiver) returns handle = @java:Method {
    name: "nullFieldIndicator",
    'class: "com.opencsv.ICSVParser",
    paramTypes: []
} external;

function com_opencsv_ICSVParser_parseLine(handle receiver, handle arg0) returns handle|error = @java:Method {
    name: "parseLine",
    'class: "com.opencsv.ICSVParser",
    paramTypes: ["java.lang.String"]
} external;

function com_opencsv_ICSVParser_parseLineMulti(handle receiver, handle arg0) returns handle|error = @java:Method {
    name: "parseLineMulti",
    'class: "com.opencsv.ICSVParser",
    paramTypes: ["java.lang.String"]
} external;

function com_opencsv_ICSVParser_parseToLine(handle receiver, handle arg0, boolean arg1) returns handle = @java:Method {
    name: "parseToLine",
    'class: "com.opencsv.ICSVParser",
    paramTypes: ["[Ljava.lang.String;", "boolean"]
} external;

function com_opencsv_ICSVParser_setErrorLocale(handle receiver, handle arg0) = @java:Method {
    name: "setErrorLocale",
    'class: "com.opencsv.ICSVParser",
    paramTypes: ["java.util.Locale"]
} external;

function com_opencsv_ICSVParser_getDEFAULT_SEPARATOR() returns int = @java:FieldGet {
    name: "DEFAULT_SEPARATOR",
    'class: "com.opencsv.ICSVParser"
} external;

function com_opencsv_ICSVParser_getINITIAL_READ_SIZE() returns int = @java:FieldGet {
    name: "INITIAL_READ_SIZE",
    'class: "com.opencsv.ICSVParser"
} external;

function com_opencsv_ICSVParser_getREAD_BUFFER_SIZE() returns int = @java:FieldGet {
    name: "READ_BUFFER_SIZE",
    'class: "com.opencsv.ICSVParser"
} external;

function com_opencsv_ICSVParser_getDEFAULT_QUOTE_CHARACTER() returns int = @java:FieldGet {
    name: "DEFAULT_QUOTE_CHARACTER",
    'class: "com.opencsv.ICSVParser"
} external;

function com_opencsv_ICSVParser_getDEFAULT_ESCAPE_CHARACTER() returns int = @java:FieldGet {
    name: "DEFAULT_ESCAPE_CHARACTER",
    'class: "com.opencsv.ICSVParser"
} external;

function com_opencsv_ICSVParser_getDEFAULT_STRICT_QUOTES() returns boolean = @java:FieldGet {
    name: "DEFAULT_STRICT_QUOTES",
    'class: "com.opencsv.ICSVParser"
} external;

function com_opencsv_ICSVParser_getDEFAULT_IGNORE_LEADING_WHITESPACE() returns boolean = @java:FieldGet {
    name: "DEFAULT_IGNORE_LEADING_WHITESPACE",
    'class: "com.opencsv.ICSVParser"
} external;

function com_opencsv_ICSVParser_getDEFAULT_IGNORE_QUOTATIONS() returns boolean = @java:FieldGet {
    name: "DEFAULT_IGNORE_QUOTATIONS",
    'class: "com.opencsv.ICSVParser"
} external;

function com_opencsv_ICSVParser_getNULL_CHARACTER() returns int = @java:FieldGet {
    name: "NULL_CHARACTER",
    'class: "com.opencsv.ICSVParser"
} external;

function com_opencsv_ICSVParser_getDEFAULT_NULL_FIELD_INDICATOR() returns handle = @java:FieldGet {
    name: "DEFAULT_NULL_FIELD_INDICATOR",
    'class: "com.opencsv.ICSVParser"
} external;

function com_opencsv_ICSVParser_getDEFAULT_BUNDLE_NAME() returns handle = @java:FieldGet {
    name: "DEFAULT_BUNDLE_NAME",
    'class: "com.opencsv.ICSVParser"
} external;

function com_opencsv_ICSVParser_getMAX_SIZE_FOR_EMPTY_FIELD() returns int = @java:FieldGet {
    name: "MAX_SIZE_FOR_EMPTY_FIELD",
    'class: "com.opencsv.ICSVParser"
} external;

function com_opencsv_ICSVParser_getNEWLINE() returns handle = @java:FieldGet {
    name: "NEWLINE",
    'class: "com.opencsv.ICSVParser"
} external;

