import ballerina/jballerina.java;
import ballerina/jballerina.java.arrays as jarrays;
import csvplus.java.util as javautil;
import csvplus.java.lang as javalang;
import csvplus.java.io as javaio;
import csvplus.java.util.'function as javautilfunction;

# Ballerina class mapping for the Java `com.opencsv.CSVReader` class.
@java:Binding {'class: "com.opencsv.CSVReader"}
public distinct class CSVReader {

    *java:JObject;
    *javalang:Object;

    # The `handle` field that stores the reference to the `com.opencsv.CSVReader` object.
    public handle jObj;

    # The init function of the Ballerina class mapping the `com.opencsv.CSVReader` Java class.
    #
    # + obj - The `handle` value containing the Java reference of the object.
    public function init(handle obj) {
        self.jObj = obj;
    }

    # The function to retrieve the string representation of the Ballerina class mapping the `com.opencsv.CSVReader` Java class.
    #
    # + return - The `string` form of the Java object instance.
    public function toString() returns string {
        return java:toString(self.jObj) ?: "null";
    }
    # The function that maps to the `close` method of `com.opencsv.CSVReader`.
    #
    # + return - The `javaio:IOException` value returning from the Java mapping.
    public function close() returns javaio:IOException? {
        error|() externalObj = com_opencsv_CSVReader_close(self.jObj);
        if (externalObj is error) {
            javaio:IOException e = error javaio:IOException(javaio:IOEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `equals` method of `com.opencsv.CSVReader`.
    #
    # + arg0 - The `javalang:Object` value required to map with the Java method parameter.
    # + return - The `boolean` value returning from the Java mapping.
    public function 'equals(javalang:Object arg0) returns boolean {
        return com_opencsv_CSVReader_equals(self.jObj, arg0.jObj);
    }

    # The function that maps to the `forEach` method of `com.opencsv.CSVReader`.
    #
    # + arg0 - The `javautilfunction:Consumer` value required to map with the Java method parameter.
    public function forEach(javautilfunction:Consumer arg0) {
        com_opencsv_CSVReader_forEach(self.jObj, arg0.jObj);
    }

    # The function that maps to the `getClass` method of `com.opencsv.CSVReader`.
    #
    # + return - The `javalang:Class` value returning from the Java mapping.
    public function getClass() returns javalang:Class {
        handle externalObj = com_opencsv_CSVReader_getClass(self.jObj);
        javalang:Class newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getLinesRead` method of `com.opencsv.CSVReader`.
    #
    # + return - The `int` value returning from the Java mapping.
    public function getLinesRead() returns int {
        return com_opencsv_CSVReader_getLinesRead(self.jObj);
    }

    # The function that maps to the `getMultilineLimit` method of `com.opencsv.CSVReader`.
    #
    # + return - The `int` value returning from the Java mapping.
    public function getMultilineLimit() returns int {
        return com_opencsv_CSVReader_getMultilineLimit(self.jObj);
    }

    # The function that maps to the `getParser` method of `com.opencsv.CSVReader`.
    #
    # + return - The `ICSVParser` value returning from the Java mapping.
    public function getParser() returns ICSVParser {
        handle externalObj = com_opencsv_CSVReader_getParser(self.jObj);
        ICSVParser newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getRecordsRead` method of `com.opencsv.CSVReader`.
    #
    # + return - The `int` value returning from the Java mapping.
    public function getRecordsRead() returns int {
        return com_opencsv_CSVReader_getRecordsRead(self.jObj);
    }

    # The function that maps to the `getSkipLines` method of `com.opencsv.CSVReader`.
    #
    # + return - The `int` value returning from the Java mapping.
    public function getSkipLines() returns int {
        return com_opencsv_CSVReader_getSkipLines(self.jObj);
    }

    # The function that maps to the `hashCode` method of `com.opencsv.CSVReader`.
    #
    # + return - The `int` value returning from the Java mapping.
    public function hashCode() returns int {
        return com_opencsv_CSVReader_hashCode(self.jObj);
    }

    # The function that maps to the `iterator` method of `com.opencsv.CSVReader`.
    #
    # + return - The `javautil:Iterator` value returning from the Java mapping.
    public function iterator() returns javautil:Iterator {
        handle externalObj = com_opencsv_CSVReader_iterator(self.jObj);
        javautil:Iterator newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `keepCarriageReturns` method of `com.opencsv.CSVReader`.
    #
    # + return - The `boolean` value returning from the Java mapping.
    public function keepCarriageReturns() returns boolean {
        return com_opencsv_CSVReader_keepCarriageReturns(self.jObj);
    }

    # The function that maps to the `notify` method of `com.opencsv.CSVReader`.
    public function notify() {
        com_opencsv_CSVReader_notify(self.jObj);
    }

    # The function that maps to the `notifyAll` method of `com.opencsv.CSVReader`.
    public function notifyAll() {
        com_opencsv_CSVReader_notifyAll(self.jObj);
    }

    # The function that maps to the `peek` method of `com.opencsv.CSVReader`.
    #
    # + return - The `string[]` or the `javaio:IOException` value returning from the Java mapping.
    public function peek() returns string[]|javaio:IOException|error {
        handle|error externalObj = com_opencsv_CSVReader_peek(self.jObj);
        if (externalObj is error) {
            javaio:IOException e = error javaio:IOException(javaio:IOEXCEPTION, externalObj, message = externalObj.message());
            return e;
        } else {
            return <string[]>check jarrays:fromHandle(externalObj, "string");
        }
    }

    # The function that maps to the `readAll` method of `com.opencsv.CSVReader`.
    #
    # + return - The `javautil:List` or the `javaio:IOException` value returning from the Java mapping.
    public function readAll() returns javautil:List|javaio:IOException {
        handle|error externalObj = com_opencsv_CSVReader_readAll(self.jObj);
        if (externalObj is error) {
            javaio:IOException e = error javaio:IOException(javaio:IOEXCEPTION, externalObj, message = externalObj.message());
            return e;
        } else {
            javautil:List newObj = new (externalObj);
            return newObj;
        }
    }


    # The function that maps to the `readNext` method of `com.opencsv.CSVReader`.
    #
    # + return - The `string[]` or the `javaio:IOException` value returning from the Java mapping.
    public function readNext() returns string[]?|javaio:IOException|error {
        handle|error externalObj = com_opencsv_CSVReader_readNext(self.jObj);
        if externalObj is error {
            return error javaio:IOException(javaio:IOEXCEPTION, externalObj, message = externalObj.message());
        }
        if java:isNull(externalObj) {
            return null;
        }
        return <string[]>check jarrays:fromHandle(externalObj, "string");
    }


    # The function that maps to the `readNextSilently` method of `com.opencsv.CSVReader`.
    #
    # + return - The `string[]` or the `javaio:IOException` value returning from the Java mapping.
    public function readNextSilently() returns string[]?|javaio:IOException|error {
        handle|error externalObj = com_opencsv_CSVReader_readNextSilently(self.jObj);
        if externalObj is error {
            return error javaio:IOException(javaio:IOEXCEPTION, externalObj, message = externalObj.message());
        }
        if java:isNull(externalObj) {
            return null;
        }
        return <string[]>check jarrays:fromHandle(externalObj, "string");
    }

    # The function that maps to the `setErrorLocale` method of `com.opencsv.CSVReader`.
    #
    # + arg0 - The `javautil:Locale` value required to map with the Java method parameter.
    public function setErrorLocale(javautil:Locale arg0) {
        com_opencsv_CSVReader_setErrorLocale(self.jObj, arg0.jObj);
    }

    # The function that maps to the `skip` method of `com.opencsv.CSVReader`.
    #
    # + arg0 - The `int` value required to map with the Java method parameter.
    # + return - The `javaio:IOException` value returning from the Java mapping.
    public function skip(int arg0) returns javaio:IOException? {
        error|() externalObj = com_opencsv_CSVReader_skip(self.jObj, arg0);
        if (externalObj is error) {
            javaio:IOException e = error javaio:IOException(javaio:IOEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `spliterator` method of `com.opencsv.CSVReader`.
    #
    # + return - The `javautil:Spliterator` value returning from the Java mapping.
    public function spliterator() returns javautil:Spliterator {
        handle externalObj = com_opencsv_CSVReader_spliterator(self.jObj);
        javautil:Spliterator newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `verifyReader` method of `com.opencsv.CSVReader`.
    #
    # + return - The `boolean` value returning from the Java mapping.
    public function verifyReader() returns boolean {
        return com_opencsv_CSVReader_verifyReader(self.jObj);
    }

    # The function that maps to the `wait` method of `com.opencsv.CSVReader`.
    #
    # + return - The `javalang:InterruptedException` value returning from the Java mapping.
    public function 'wait() returns javalang:InterruptedException? {
        error|() externalObj = com_opencsv_CSVReader_wait(self.jObj);
        if (externalObj is error) {
            javalang:InterruptedException e = error javalang:InterruptedException(javalang:INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `wait` method of `com.opencsv.CSVReader`.
    #
    # + arg0 - The `int` value required to map with the Java method parameter.
    # + return - The `javalang:InterruptedException` value returning from the Java mapping.
    public function wait2(int arg0) returns javalang:InterruptedException? {
        error|() externalObj = com_opencsv_CSVReader_wait2(self.jObj, arg0);
        if (externalObj is error) {
            javalang:InterruptedException e = error javalang:InterruptedException(javalang:INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `wait` method of `com.opencsv.CSVReader`.
    #
    # + arg0 - The `int` value required to map with the Java method parameter.
    # + arg1 - The `int` value required to map with the Java method parameter.
    # + return - The `javalang:InterruptedException` value returning from the Java mapping.
    public function wait3(int arg0, int arg1) returns javalang:InterruptedException? {
        error|() externalObj = com_opencsv_CSVReader_wait3(self.jObj, arg0, arg1);
        if (externalObj is error) {
            javalang:InterruptedException e = error javalang:InterruptedException(javalang:INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

}

# The constructor function to generate an object of `com.opencsv.CSVReader`.
#
# + arg0 - The `javaio:Reader` value required to map with the Java constructor parameter.
# + return - The new `CSVReader` class generated.
public function newCSVReader1(javaio:Reader arg0) returns CSVReader {
    handle externalObj = com_opencsv_CSVReader_newCSVReader1(arg0.jObj);
    CSVReader newObj = new (externalObj);
    return newObj;
}

# The function that retrieves the value of the public field `DEFAULT_KEEP_CR`.
#
# + return - The `boolean` value of the field.
public function CSVReader_getDEFAULT_KEEP_CR() returns boolean {
    return com_opencsv_CSVReader_getDEFAULT_KEEP_CR();
}

# The function that retrieves the value of the public field `DEFAULT_VERIFY_READER`.
#
# + return - The `boolean` value of the field.
public function CSVReader_getDEFAULT_VERIFY_READER() returns boolean {
    return com_opencsv_CSVReader_getDEFAULT_VERIFY_READER();
}

# The function that retrieves the value of the public field `DEFAULT_SKIP_LINES`.
#
# + return - The `int` value of the field.
public function CSVReader_getDEFAULT_SKIP_LINES() returns int {
    return com_opencsv_CSVReader_getDEFAULT_SKIP_LINES();
}

# The function that retrieves the value of the public field `DEFAULT_MULTILINE_LIMIT`.
#
# + return - The `int` value of the field.
public function CSVReader_getDEFAULT_MULTILINE_LIMIT() returns int {
    return com_opencsv_CSVReader_getDEFAULT_MULTILINE_LIMIT();
}

# The function that retrieves the value of the public field `READ_AHEAD_LIMIT`.
#
# + return - The `int` value of the field.
public function CSVReader_getREAD_AHEAD_LIMIT() returns int {
    return com_opencsv_CSVReader_getREAD_AHEAD_LIMIT();
}

function com_opencsv_CSVReader_close(handle receiver) returns error? = @java:Method {
    name: "close",
    'class: "com.opencsv.CSVReader",
    paramTypes: []
} external;

function com_opencsv_CSVReader_equals(handle receiver, handle arg0) returns boolean = @java:Method {
    name: "equals",
    'class: "com.opencsv.CSVReader",
    paramTypes: ["java.lang.Object"]
} external;

function com_opencsv_CSVReader_forEach(handle receiver, handle arg0) = @java:Method {
    name: "forEach",
    'class: "com.opencsv.CSVReader",
    paramTypes: ["java.util.function.Consumer"]
} external;

function com_opencsv_CSVReader_getClass(handle receiver) returns handle = @java:Method {
    name: "getClass",
    'class: "com.opencsv.CSVReader",
    paramTypes: []
} external;

function com_opencsv_CSVReader_getLinesRead(handle receiver) returns int = @java:Method {
    name: "getLinesRead",
    'class: "com.opencsv.CSVReader",
    paramTypes: []
} external;

function com_opencsv_CSVReader_getMultilineLimit(handle receiver) returns int = @java:Method {
    name: "getMultilineLimit",
    'class: "com.opencsv.CSVReader",
    paramTypes: []
} external;

function com_opencsv_CSVReader_getParser(handle receiver) returns handle = @java:Method {
    name: "getParser",
    'class: "com.opencsv.CSVReader",
    paramTypes: []
} external;

function com_opencsv_CSVReader_getRecordsRead(handle receiver) returns int = @java:Method {
    name: "getRecordsRead",
    'class: "com.opencsv.CSVReader",
    paramTypes: []
} external;

function com_opencsv_CSVReader_getSkipLines(handle receiver) returns int = @java:Method {
    name: "getSkipLines",
    'class: "com.opencsv.CSVReader",
    paramTypes: []
} external;

function com_opencsv_CSVReader_hashCode(handle receiver) returns int = @java:Method {
    name: "hashCode",
    'class: "com.opencsv.CSVReader",
    paramTypes: []
} external;

function com_opencsv_CSVReader_iterator(handle receiver) returns handle = @java:Method {
    name: "iterator",
    'class: "com.opencsv.CSVReader",
    paramTypes: []
} external;

function com_opencsv_CSVReader_keepCarriageReturns(handle receiver) returns boolean = @java:Method {
    name: "keepCarriageReturns",
    'class: "com.opencsv.CSVReader",
    paramTypes: []
} external;

function com_opencsv_CSVReader_notify(handle receiver) = @java:Method {
    name: "notify",
    'class: "com.opencsv.CSVReader",
    paramTypes: []
} external;

function com_opencsv_CSVReader_notifyAll(handle receiver) = @java:Method {
    name: "notifyAll",
    'class: "com.opencsv.CSVReader",
    paramTypes: []
} external;

function com_opencsv_CSVReader_peek(handle receiver) returns handle|error = @java:Method {
    name: "peek",
    'class: "com.opencsv.CSVReader",
    paramTypes: []
} external;

function com_opencsv_CSVReader_readAll(handle receiver) returns handle|error = @java:Method {
    name: "readAll",
    'class: "com.opencsv.CSVReader",
    paramTypes: []
} external;

function com_opencsv_CSVReader_readNext(handle receiver) returns handle|error = @java:Method {
    name: "readNext",
    'class: "com.opencsv.CSVReader",
    paramTypes: []
} external;

function com_opencsv_CSVReader_readNextSilently(handle receiver) returns handle|error = @java:Method {
    name: "readNextSilently",
    'class: "com.opencsv.CSVReader",
    paramTypes: []
} external;

function com_opencsv_CSVReader_setErrorLocale(handle receiver, handle arg0) = @java:Method {
    name: "setErrorLocale",
    'class: "com.opencsv.CSVReader",
    paramTypes: ["java.util.Locale"]
} external;

function com_opencsv_CSVReader_skip(handle receiver, int arg0) returns error? = @java:Method {
    name: "skip",
    'class: "com.opencsv.CSVReader",
    paramTypes: ["int"]
} external;

function com_opencsv_CSVReader_spliterator(handle receiver) returns handle = @java:Method {
    name: "spliterator",
    'class: "com.opencsv.CSVReader",
    paramTypes: []
} external;

function com_opencsv_CSVReader_verifyReader(handle receiver) returns boolean = @java:Method {
    name: "verifyReader",
    'class: "com.opencsv.CSVReader",
    paramTypes: []
} external;

function com_opencsv_CSVReader_wait(handle receiver) returns error? = @java:Method {
    name: "wait",
    'class: "com.opencsv.CSVReader",
    paramTypes: []
} external;

function com_opencsv_CSVReader_wait2(handle receiver, int arg0) returns error? = @java:Method {
    name: "wait",
    'class: "com.opencsv.CSVReader",
    paramTypes: ["long"]
} external;

function com_opencsv_CSVReader_wait3(handle receiver, int arg0, int arg1) returns error? = @java:Method {
    name: "wait",
    'class: "com.opencsv.CSVReader",
    paramTypes: ["long", "int"]
} external;

function com_opencsv_CSVReader_getDEFAULT_KEEP_CR() returns boolean = @java:FieldGet {
    name: "DEFAULT_KEEP_CR",
    'class: "com.opencsv.CSVReader"
} external;

function com_opencsv_CSVReader_getDEFAULT_VERIFY_READER() returns boolean = @java:FieldGet {
    name: "DEFAULT_VERIFY_READER",
    'class: "com.opencsv.CSVReader"
} external;

function com_opencsv_CSVReader_getDEFAULT_SKIP_LINES() returns int = @java:FieldGet {
    name: "DEFAULT_SKIP_LINES",
    'class: "com.opencsv.CSVReader"
} external;

function com_opencsv_CSVReader_getDEFAULT_MULTILINE_LIMIT() returns int = @java:FieldGet {
    name: "DEFAULT_MULTILINE_LIMIT",
    'class: "com.opencsv.CSVReader"
} external;

function com_opencsv_CSVReader_getREAD_AHEAD_LIMIT() returns int = @java:FieldGet {
    name: "READ_AHEAD_LIMIT",
    'class: "com.opencsv.CSVReader"
} external;

function com_opencsv_CSVReader_newCSVReader1(handle arg0) returns handle = @java:Constructor {
    'class: "com.opencsv.CSVReader",
    paramTypes: ["java.io.Reader"]
} external;

