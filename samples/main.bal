import ballerina/io;

final string USAGE = string`usage: bal run -- help | <number> 
where <number> is a sample number. Supported numbers:
     1  # just a hello world sample
     2  # sample for CSVParser which parses a single line
`;

function usage() {
    io:println(USAGE);
}

function isNumeric(string s) returns boolean {
    match int:fromString(s) {
        var i if i is int => {return true;}
        var e if e is error => {return false;}
    }
    // just to keep the compiler happy. Shouldn't be
    // reachable.
    return false;
}

public function main(string... args) returns error? {
    if args.length() == 0 {
        io:println("fatal: missing sample number");
        usage();
        return error("command line error");
    }    

    string sample = args[0].toLowerAscii().trim();
    if isNumeric(sample) {
        int value = check int:fromString(sample);
        sample = value.toString();
    }

    match sample {
        // BUG: can't use -h or --help. bal run doesn't pass these options
        // to the ballerina program
        "help" => {
            usage();
            return;
        }

        "1" => { check sample01();}
        "2" => { check sample02();}

        _ => {
            io:println(`fatal: unsupported sample '${sample}`);
            usage();
            return error("command line error"); 
        }
    }
}