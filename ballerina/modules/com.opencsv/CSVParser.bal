import ballerina/jballerina.java;
import ballerina/jballerina.java.arrays as jarrays;
import csvplus.java.lang as javalang;
import csvplus.java.util as javautil;
import csvplus.com.opencsv.enums as comopencsvenums;
import csvplus.java.io as javaio;

# Ballerina class mapping for the Java `com.opencsv.CSVParser` class.
@java:Binding {'class: "com.opencsv.CSVParser"}
public distinct class CSVParser {

    *java:JObject;
    *AbstractCSVParser;

    # The `handle` field that stores the reference to the `com.opencsv.CSVParser` object.
    public handle jObj;

    # The init function of the Ballerina class mapping the `com.opencsv.CSVParser` Java class.
    #
    # + obj - The `handle` value containing the Java reference of the object.
    public function init(handle obj) {
        self.jObj = obj;
    }

    # The function to retrieve the string representation of the Ballerina class mapping the `com.opencsv.CSVParser` Java class.
    #
    # + return - The `string` form of the Java object instance.
    public function toString() returns string {
        return java:toString(self.jObj) ?: "null";
    }
    # The function that maps to the `equals` method of `com.opencsv.CSVParser`.
    #
    # + arg0 - The `javalang:Object` value required to map with the Java method parameter.
    # + return - The `boolean` value returning from the Java mapping.
    public function 'equals(javalang:Object arg0) returns boolean {
        return com_opencsv_CSVParser_equals(self.jObj, arg0.jObj);
    }

    # The function that maps to the `getClass` method of `com.opencsv.CSVParser`.
    #
    # + return - The `javalang:Class` value returning from the Java mapping.
    public function getClass() returns javalang:Class {
        handle externalObj = com_opencsv_CSVParser_getClass(self.jObj);
        javalang:Class newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getEscape` method of `com.opencsv.CSVParser`.
    #
    # + return - The `int` value returning from the Java mapping.
    public function getEscape() returns int {
        return com_opencsv_CSVParser_getEscape(self.jObj);
    }

    # The function that maps to the `getPendingText` method of `com.opencsv.CSVParser`.
    #
    # + return - The `string` value returning from the Java mapping.
    public function getPendingText() returns string? {
        return java:toString(com_opencsv_CSVParser_getPendingText(self.jObj));
    }

    # The function that maps to the `getQuotechar` method of `com.opencsv.CSVParser`.
    #
    # + return - The `int` value returning from the Java mapping.
    public function getQuotechar() returns int {
        return com_opencsv_CSVParser_getQuotechar(self.jObj);
    }

    # The function that maps to the `getSeparator` method of `com.opencsv.CSVParser`.
    #
    # + return - The `int` value returning from the Java mapping.
    public function getSeparator() returns int {
        return com_opencsv_CSVParser_getSeparator(self.jObj);
    }

    # The function that maps to the `hashCode` method of `com.opencsv.CSVParser`.
    #
    # + return - The `int` value returning from the Java mapping.
    public function hashCode() returns int {
        return com_opencsv_CSVParser_hashCode(self.jObj);
    }

    # The function that maps to the `isIgnoreLeadingWhiteSpace` method of `com.opencsv.CSVParser`.
    #
    # + return - The `boolean` value returning from the Java mapping.
    public function isIgnoreLeadingWhiteSpace() returns boolean {
        return com_opencsv_CSVParser_isIgnoreLeadingWhiteSpace(self.jObj);
    }

    # The function that maps to the `isIgnoreQuotations` method of `com.opencsv.CSVParser`.
    #
    # + return - The `boolean` value returning from the Java mapping.
    public function isIgnoreQuotations() returns boolean {
        return com_opencsv_CSVParser_isIgnoreQuotations(self.jObj);
    }

    # The function that maps to the `isPending` method of `com.opencsv.CSVParser`.
    #
    # + return - The `boolean` value returning from the Java mapping.
    public function isPending() returns boolean {
        return com_opencsv_CSVParser_isPending(self.jObj);
    }

    # The function that maps to the `isStrictQuotes` method of `com.opencsv.CSVParser`.
    #
    # + return - The `boolean` value returning from the Java mapping.
    public function isStrictQuotes() returns boolean {
        return com_opencsv_CSVParser_isStrictQuotes(self.jObj);
    }

    # The function that maps to the `notify` method of `com.opencsv.CSVParser`.
    public function notify() {
        com_opencsv_CSVParser_notify(self.jObj);
    }

    # The function that maps to the `notifyAll` method of `com.opencsv.CSVParser`.
    public function notifyAll() {
        com_opencsv_CSVParser_notifyAll(self.jObj);
    }

    # The function that maps to the `nullFieldIndicator` method of `com.opencsv.CSVParser`.
    #
    # + return - The `comopencsvenums:CSVReaderNullFieldIndicator` value returning from the Java mapping.
    public function nullFieldIndicator() returns comopencsvenums:CSVReaderNullFieldIndicator {
        handle externalObj = com_opencsv_CSVParser_nullFieldIndicator(self.jObj);
        comopencsvenums:CSVReaderNullFieldIndicator newObj = new (externalObj);
        return newObj;
    }


    # The function that maps to the `parseLine` method of `com.opencsv.AbstractCSVParser`.
    #
    # + arg0 - The `string` value required to map with the Java method parameter.
    # + return - The `string?[]` or the `javaio:IOException` value returning from the Java mapping.
    public function parseLine(string arg0) returns string?[]|javaio:IOException|error {
        handle|error externalObj = com_opencsv_CSVParser_parseLine(self.jObj, java:fromString(arg0));
        if externalObj is error {
            return error javaio:IOException(javaio:IOEXCEPTION, externalObj, message = externalObj.message());
        } 
        any[] values = check jarrays:fromHandle(externalObj, "handle");
        return values.map(v => java:toString(<handle>v));
    }

    # The function that maps to the `parseLineMulti` method of `com.opencsv.CSVParser`.
    #
    # + arg0 - The `string` value required to map with the Java method parameter.
    # + return - The `string[]` or the `javaio:IOException` value returning from the Java mapping.
    public function parseLineMulti(string arg0) returns string[]|javaio:IOException|error {
        handle|error externalObj = com_opencsv_CSVParser_parseLineMulti(self.jObj, java:fromString(arg0));
        if (externalObj is error) {
            javaio:IOException e = error javaio:IOException(javaio:IOEXCEPTION, externalObj, message = externalObj.message());
            return e;
        } else {
            return <string[]>check jarrays:fromHandle(externalObj, "string");
        }
    }

    # The function that maps to the `parseToLine` method of `com.opencsv.CSVParser`.
    #
    # + arg0 - The `string[]` value required to map with the Java method parameter.
    # + arg1 - The `boolean` value required to map with the Java method parameter.
    # + return - The `string` value returning from the Java mapping.
    public function parseToLine(string[] arg0, boolean arg1) returns string?|error {
        return java:toString(com_opencsv_CSVParser_parseToLine(self.jObj, check jarrays:toHandle(arg0, "java.lang.String"), arg1));
    }

    # The function that maps to the `setErrorLocale` method of `com.opencsv.CSVParser`.
    #
    # + arg0 - The `javautil:Locale` value required to map with the Java method parameter.
    public function setErrorLocale(javautil:Locale arg0) {
        com_opencsv_CSVParser_setErrorLocale(self.jObj, arg0.jObj);
    }

    # The function that maps to the `wait` method of `com.opencsv.CSVParser`.
    #
    # + return - The `javalang:InterruptedException` value returning from the Java mapping.
    public function 'wait() returns javalang:InterruptedException? {
        error|() externalObj = com_opencsv_CSVParser_wait(self.jObj);
        if (externalObj is error) {
            javalang:InterruptedException e = error javalang:InterruptedException(javalang:INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `wait` method of `com.opencsv.CSVParser`.
    #
    # + arg0 - The `int` value required to map with the Java method parameter.
    # + return - The `javalang:InterruptedException` value returning from the Java mapping.
    public function wait2(int arg0) returns javalang:InterruptedException? {
        error|() externalObj = com_opencsv_CSVParser_wait2(self.jObj, arg0);
        if (externalObj is error) {
            javalang:InterruptedException e = error javalang:InterruptedException(javalang:INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `wait` method of `com.opencsv.CSVParser`.
    #
    # + arg0 - The `int` value required to map with the Java method parameter.
    # + arg1 - The `int` value required to map with the Java method parameter.
    # + return - The `javalang:InterruptedException` value returning from the Java mapping.
    public function wait3(int arg0, int arg1) returns javalang:InterruptedException? {
        error|() externalObj = com_opencsv_CSVParser_wait3(self.jObj, arg0, arg1);
        if (externalObj is error) {
            javalang:InterruptedException e = error javalang:InterruptedException(javalang:INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

}

# The constructor function to generate an object of `com.opencsv.CSVParser`.
#
# + return - The new `CSVParser` class generated.
public function newCSVParser1() returns CSVParser {
    handle externalObj = com_opencsv_CSVParser_newCSVParser1();
    CSVParser newObj = new (externalObj);
    return newObj;
}

# The function that retrieves the value of the public field `DEFAULT_SEPARATOR`.
#
# + return - The `int` value of the field.
public function CSVParser_getDEFAULT_SEPARATOR() returns int {
    return com_opencsv_CSVParser_getDEFAULT_SEPARATOR();
}

# The function that retrieves the value of the public field `INITIAL_READ_SIZE`.
#
# + return - The `int` value of the field.
public function CSVParser_getINITIAL_READ_SIZE() returns int {
    return com_opencsv_CSVParser_getINITIAL_READ_SIZE();
}

# The function that retrieves the value of the public field `READ_BUFFER_SIZE`.
#
# + return - The `int` value of the field.
public function CSVParser_getREAD_BUFFER_SIZE() returns int {
    return com_opencsv_CSVParser_getREAD_BUFFER_SIZE();
}

# The function that retrieves the value of the public field `DEFAULT_QUOTE_CHARACTER`.
#
# + return - The `int` value of the field.
public function CSVParser_getDEFAULT_QUOTE_CHARACTER() returns int {
    return com_opencsv_CSVParser_getDEFAULT_QUOTE_CHARACTER();
}

# The function that retrieves the value of the public field `DEFAULT_ESCAPE_CHARACTER`.
#
# + return - The `int` value of the field.
public function CSVParser_getDEFAULT_ESCAPE_CHARACTER() returns int {
    return com_opencsv_CSVParser_getDEFAULT_ESCAPE_CHARACTER();
}

# The function that retrieves the value of the public field `DEFAULT_STRICT_QUOTES`.
#
# + return - The `boolean` value of the field.
public function CSVParser_getDEFAULT_STRICT_QUOTES() returns boolean {
    return com_opencsv_CSVParser_getDEFAULT_STRICT_QUOTES();
}

# The function that retrieves the value of the public field `DEFAULT_IGNORE_LEADING_WHITESPACE`.
#
# + return - The `boolean` value of the field.
public function CSVParser_getDEFAULT_IGNORE_LEADING_WHITESPACE() returns boolean {
    return com_opencsv_CSVParser_getDEFAULT_IGNORE_LEADING_WHITESPACE();
}

# The function that retrieves the value of the public field `DEFAULT_IGNORE_QUOTATIONS`.
#
# + return - The `boolean` value of the field.
public function CSVParser_getDEFAULT_IGNORE_QUOTATIONS() returns boolean {
    return com_opencsv_CSVParser_getDEFAULT_IGNORE_QUOTATIONS();
}

# The function that retrieves the value of the public field `NULL_CHARACTER`.
#
# + return - The `int` value of the field.
public function CSVParser_getNULL_CHARACTER() returns int {
    return com_opencsv_CSVParser_getNULL_CHARACTER();
}

# The function that retrieves the value of the public field `DEFAULT_NULL_FIELD_INDICATOR`.
#
# + return - The `CSVReaderNullFieldIndicator` value of the field.
public function CSVParser_getDEFAULT_NULL_FIELD_INDICATOR() returns comopencsvenums:CSVReaderNullFieldIndicator {
    handle externalObj = com_opencsv_CSVParser_getDEFAULT_NULL_FIELD_INDICATOR();
    comopencsvenums:CSVReaderNullFieldIndicator newObj = new (externalObj);
    return newObj;
}

# The function that retrieves the value of the public field `DEFAULT_BUNDLE_NAME`.
#
# + return - The `string` value of the field.
public function CSVParser_getDEFAULT_BUNDLE_NAME() returns string? {
    return java:toString(com_opencsv_CSVParser_getDEFAULT_BUNDLE_NAME());
}

# The function that retrieves the value of the public field `MAX_SIZE_FOR_EMPTY_FIELD`.
#
# + return - The `int` value of the field.
public function CSVParser_getMAX_SIZE_FOR_EMPTY_FIELD() returns int {
    return com_opencsv_CSVParser_getMAX_SIZE_FOR_EMPTY_FIELD();
}

# The function that retrieves the value of the public field `NEWLINE`.
#
# + return - The `string` value of the field.
public function CSVParser_getNEWLINE() returns string? {
    return java:toString(com_opencsv_CSVParser_getNEWLINE());
}

function com_opencsv_CSVParser_equals(handle receiver, handle arg0) returns boolean = @java:Method {
    name: "equals",
    'class: "com.opencsv.CSVParser",
    paramTypes: ["java.lang.Object"]
} external;

function com_opencsv_CSVParser_getClass(handle receiver) returns handle = @java:Method {
    name: "getClass",
    'class: "com.opencsv.CSVParser",
    paramTypes: []
} external;

function com_opencsv_CSVParser_getEscape(handle receiver) returns int = @java:Method {
    name: "getEscape",
    'class: "com.opencsv.CSVParser",
    paramTypes: []
} external;

function com_opencsv_CSVParser_getPendingText(handle receiver) returns handle = @java:Method {
    name: "getPendingText",
    'class: "com.opencsv.CSVParser",
    paramTypes: []
} external;

function com_opencsv_CSVParser_getQuotechar(handle receiver) returns int = @java:Method {
    name: "getQuotechar",
    'class: "com.opencsv.CSVParser",
    paramTypes: []
} external;

function com_opencsv_CSVParser_getSeparator(handle receiver) returns int = @java:Method {
    name: "getSeparator",
    'class: "com.opencsv.CSVParser",
    paramTypes: []
} external;

function com_opencsv_CSVParser_hashCode(handle receiver) returns int = @java:Method {
    name: "hashCode",
    'class: "com.opencsv.CSVParser",
    paramTypes: []
} external;

function com_opencsv_CSVParser_isIgnoreLeadingWhiteSpace(handle receiver) returns boolean = @java:Method {
    name: "isIgnoreLeadingWhiteSpace",
    'class: "com.opencsv.CSVParser",
    paramTypes: []
} external;

function com_opencsv_CSVParser_isIgnoreQuotations(handle receiver) returns boolean = @java:Method {
    name: "isIgnoreQuotations",
    'class: "com.opencsv.CSVParser",
    paramTypes: []
} external;

function com_opencsv_CSVParser_isPending(handle receiver) returns boolean = @java:Method {
    name: "isPending",
    'class: "com.opencsv.CSVParser",
    paramTypes: []
} external;

function com_opencsv_CSVParser_isStrictQuotes(handle receiver) returns boolean = @java:Method {
    name: "isStrictQuotes",
    'class: "com.opencsv.CSVParser",
    paramTypes: []
} external;

function com_opencsv_CSVParser_notify(handle receiver) = @java:Method {
    name: "notify",
    'class: "com.opencsv.CSVParser",
    paramTypes: []
} external;

function com_opencsv_CSVParser_notifyAll(handle receiver) = @java:Method {
    name: "notifyAll",
    'class: "com.opencsv.CSVParser",
    paramTypes: []
} external;

function com_opencsv_CSVParser_nullFieldIndicator(handle receiver) returns handle = @java:Method {
    name: "nullFieldIndicator",
    'class: "com.opencsv.CSVParser",
    paramTypes: []
} external;

function com_opencsv_CSVParser_parseLine(handle receiver, handle arg0) returns handle|error = @java:Method {
    name: "parseLine",
    'class: "com.opencsv.CSVParser",
    paramTypes: ["java.lang.String"]
} external;

function com_opencsv_CSVParser_parseLineMulti(handle receiver, handle arg0) returns handle|error = @java:Method {
    name: "parseLineMulti",
    'class: "com.opencsv.CSVParser",
    paramTypes: ["java.lang.String"]
} external;

function com_opencsv_CSVParser_parseToLine(handle receiver, handle arg0, boolean arg1) returns handle = @java:Method {
    name: "parseToLine",
    'class: "com.opencsv.CSVParser",
    paramTypes: ["[Ljava.lang.String;", "boolean"]
} external;

function com_opencsv_CSVParser_setErrorLocale(handle receiver, handle arg0) = @java:Method {
    name: "setErrorLocale",
    'class: "com.opencsv.CSVParser",
    paramTypes: ["java.util.Locale"]
} external;

function com_opencsv_CSVParser_wait(handle receiver) returns error? = @java:Method {
    name: "wait",
    'class: "com.opencsv.CSVParser",
    paramTypes: []
} external;

function com_opencsv_CSVParser_wait2(handle receiver, int arg0) returns error? = @java:Method {
    name: "wait",
    'class: "com.opencsv.CSVParser",
    paramTypes: ["long"]
} external;

function com_opencsv_CSVParser_wait3(handle receiver, int arg0, int arg1) returns error? = @java:Method {
    name: "wait",
    'class: "com.opencsv.CSVParser",
    paramTypes: ["long", "int"]
} external;

function com_opencsv_CSVParser_getDEFAULT_SEPARATOR() returns int = @java:FieldGet {
    name: "DEFAULT_SEPARATOR",
    'class: "com.opencsv.CSVParser"
} external;

function com_opencsv_CSVParser_getINITIAL_READ_SIZE() returns int = @java:FieldGet {
    name: "INITIAL_READ_SIZE",
    'class: "com.opencsv.CSVParser"
} external;

function com_opencsv_CSVParser_getREAD_BUFFER_SIZE() returns int = @java:FieldGet {
    name: "READ_BUFFER_SIZE",
    'class: "com.opencsv.CSVParser"
} external;

function com_opencsv_CSVParser_getDEFAULT_QUOTE_CHARACTER() returns int = @java:FieldGet {
    name: "DEFAULT_QUOTE_CHARACTER",
    'class: "com.opencsv.CSVParser"
} external;

function com_opencsv_CSVParser_getDEFAULT_ESCAPE_CHARACTER() returns int = @java:FieldGet {
    name: "DEFAULT_ESCAPE_CHARACTER",
    'class: "com.opencsv.CSVParser"
} external;

function com_opencsv_CSVParser_getDEFAULT_STRICT_QUOTES() returns boolean = @java:FieldGet {
    name: "DEFAULT_STRICT_QUOTES",
    'class: "com.opencsv.CSVParser"
} external;

function com_opencsv_CSVParser_getDEFAULT_IGNORE_LEADING_WHITESPACE() returns boolean = @java:FieldGet {
    name: "DEFAULT_IGNORE_LEADING_WHITESPACE",
    'class: "com.opencsv.CSVParser"
} external;

function com_opencsv_CSVParser_getDEFAULT_IGNORE_QUOTATIONS() returns boolean = @java:FieldGet {
    name: "DEFAULT_IGNORE_QUOTATIONS",
    'class: "com.opencsv.CSVParser"
} external;

function com_opencsv_CSVParser_getNULL_CHARACTER() returns int = @java:FieldGet {
    name: "NULL_CHARACTER",
    'class: "com.opencsv.CSVParser"
} external;

function com_opencsv_CSVParser_getDEFAULT_NULL_FIELD_INDICATOR() returns handle = @java:FieldGet {
    name: "DEFAULT_NULL_FIELD_INDICATOR",
    'class: "com.opencsv.CSVParser"
} external;

function com_opencsv_CSVParser_getDEFAULT_BUNDLE_NAME() returns handle = @java:FieldGet {
    name: "DEFAULT_BUNDLE_NAME",
    'class: "com.opencsv.CSVParser"
} external;

function com_opencsv_CSVParser_getMAX_SIZE_FOR_EMPTY_FIELD() returns int = @java:FieldGet {
    name: "MAX_SIZE_FOR_EMPTY_FIELD",
    'class: "com.opencsv.CSVParser"
} external;

function com_opencsv_CSVParser_getNEWLINE() returns handle = @java:FieldGet {
    name: "NEWLINE",
    'class: "com.opencsv.CSVParser"
} external;

function com_opencsv_CSVParser_newCSVParser1() returns handle = @java:Constructor {
    'class: "com.opencsv.CSVParser",
    paramTypes: []
} external;

