
import gubaer/csvplus.com.opencsv as csv;
import ballerina/io;

function sample02() returns error? {
    csv:CSVParserBuilder builder = csv:newCSVParserBuilder1();
    csv:CSVParser parser = builder.build();

    final string line = "value a, value b, value c";
    var result = check parser.parseLine(line);
    io:println("parsed CSV values:");
    foreach  string value  in result {
        io:println("   " + value);
    }
}