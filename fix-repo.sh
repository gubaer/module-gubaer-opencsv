
if [ -d .git_old ]; then
    echo "removing obsolete .git_old ..."
    rm -rf .git_old
fi

mv -v .git .git_old
git init 
git remote add origin git@bitbucket.org:gubaer/module-gubaer-opencsv.git
git fetch 
git reset origin/master --mixed 