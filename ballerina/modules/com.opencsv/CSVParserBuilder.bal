import ballerina/jballerina.java;
import csvplus.java.lang as javalang;
import csvplus.java.util as javautil;
import csvplus.com.opencsv.enums as comopencsvenums;

# Ballerina class mapping for the Java `com.opencsv.CSVParserBuilder` class.
@java:Binding {'class: "com.opencsv.CSVParserBuilder"}
public distinct class CSVParserBuilder {

    *java:JObject;
    *javalang:Object;

    # The `handle` field that stores the reference to the `com.opencsv.CSVParserBuilder` object.
    public handle jObj;

    # The init function of the Ballerina class mapping the `com.opencsv.CSVParserBuilder` Java class.
    #
    # + obj - The `handle` value containing the Java reference of the object.
    public function init(handle obj) {
        self.jObj = obj;
    }

    # The function to retrieve the string representation of the Ballerina class mapping the `com.opencsv.CSVParserBuilder` Java class.
    #
    # + return - The `string` form of the Java object instance.
    public function toString() returns string {
        return java:toString(self.jObj) ?: "null";
    }
    # The function that maps to the `build` method of `com.opencsv.CSVParserBuilder`.
    #
    # + return - The `CSVParser` value returning from the Java mapping.
    public function build() returns CSVParser {
        handle externalObj = com_opencsv_CSVParserBuilder_build(self.jObj);
        CSVParser newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `equals` method of `com.opencsv.CSVParserBuilder`.
    #
    # + arg0 - The `javalang:Object` value required to map with the Java method parameter.
    # + return - The `boolean` value returning from the Java mapping.
    public function 'equals(javalang:Object arg0) returns boolean {
        return com_opencsv_CSVParserBuilder_equals(self.jObj, arg0.jObj);
    }

    # The function that maps to the `getClass` method of `com.opencsv.CSVParserBuilder`.
    #
    # + return - The `javalang:Class` value returning from the Java mapping.
    public function getClass() returns javalang:Class {
        handle externalObj = com_opencsv_CSVParserBuilder_getClass(self.jObj);
        javalang:Class newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getEscapeChar` method of `com.opencsv.CSVParserBuilder`.
    #
    # + return - The `int` value returning from the Java mapping.
    public function getEscapeChar() returns int {
        return com_opencsv_CSVParserBuilder_getEscapeChar(self.jObj);
    }

    # The function that maps to the `getQuoteChar` method of `com.opencsv.CSVParserBuilder`.
    #
    # + return - The `int` value returning from the Java mapping.
    public function getQuoteChar() returns int {
        return com_opencsv_CSVParserBuilder_getQuoteChar(self.jObj);
    }

    # The function that maps to the `getSeparator` method of `com.opencsv.CSVParserBuilder`.
    #
    # + return - The `int` value returning from the Java mapping.
    public function getSeparator() returns int {
        return com_opencsv_CSVParserBuilder_getSeparator(self.jObj);
    }

    # The function that maps to the `hashCode` method of `com.opencsv.CSVParserBuilder`.
    #
    # + return - The `int` value returning from the Java mapping.
    public function hashCode() returns int {
        return com_opencsv_CSVParserBuilder_hashCode(self.jObj);
    }

    # The function that maps to the `isIgnoreLeadingWhiteSpace` method of `com.opencsv.CSVParserBuilder`.
    #
    # + return - The `boolean` value returning from the Java mapping.
    public function isIgnoreLeadingWhiteSpace() returns boolean {
        return com_opencsv_CSVParserBuilder_isIgnoreLeadingWhiteSpace(self.jObj);
    }

    # The function that maps to the `isIgnoreQuotations` method of `com.opencsv.CSVParserBuilder`.
    #
    # + return - The `boolean` value returning from the Java mapping.
    public function isIgnoreQuotations() returns boolean {
        return com_opencsv_CSVParserBuilder_isIgnoreQuotations(self.jObj);
    }

    # The function that maps to the `isStrictQuotes` method of `com.opencsv.CSVParserBuilder`.
    #
    # + return - The `boolean` value returning from the Java mapping.
    public function isStrictQuotes() returns boolean {
        return com_opencsv_CSVParserBuilder_isStrictQuotes(self.jObj);
    }

    # The function that maps to the `notify` method of `com.opencsv.CSVParserBuilder`.
    public function notify() {
        com_opencsv_CSVParserBuilder_notify(self.jObj);
    }

    # The function that maps to the `notifyAll` method of `com.opencsv.CSVParserBuilder`.
    public function notifyAll() {
        com_opencsv_CSVParserBuilder_notifyAll(self.jObj);
    }

    # The function that maps to the `nullFieldIndicator` method of `com.opencsv.CSVParserBuilder`.
    #
    # + return - The `comopencsvenums:CSVReaderNullFieldIndicator` value returning from the Java mapping.
    public function nullFieldIndicator() returns comopencsvenums:CSVReaderNullFieldIndicator {
        handle externalObj = com_opencsv_CSVParserBuilder_nullFieldIndicator(self.jObj);
        comopencsvenums:CSVReaderNullFieldIndicator newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `wait` method of `com.opencsv.CSVParserBuilder`.
    #
    # + return - The `javalang:InterruptedException` value returning from the Java mapping.
    public function 'wait() returns javalang:InterruptedException? {
        error|() externalObj = com_opencsv_CSVParserBuilder_wait(self.jObj);
        if (externalObj is error) {
            javalang:InterruptedException e = error javalang:InterruptedException(javalang:INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `wait` method of `com.opencsv.CSVParserBuilder`.
    #
    # + arg0 - The `int` value required to map with the Java method parameter.
    # + return - The `javalang:InterruptedException` value returning from the Java mapping.
    public function wait2(int arg0) returns javalang:InterruptedException? {
        error|() externalObj = com_opencsv_CSVParserBuilder_wait2(self.jObj, arg0);
        if (externalObj is error) {
            javalang:InterruptedException e = error javalang:InterruptedException(javalang:INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `wait` method of `com.opencsv.CSVParserBuilder`.
    #
    # + arg0 - The `int` value required to map with the Java method parameter.
    # + arg1 - The `int` value required to map with the Java method parameter.
    # + return - The `javalang:InterruptedException` value returning from the Java mapping.
    public function wait3(int arg0, int arg1) returns javalang:InterruptedException? {
        error|() externalObj = com_opencsv_CSVParserBuilder_wait3(self.jObj, arg0, arg1);
        if (externalObj is error) {
            javalang:InterruptedException e = error javalang:InterruptedException(javalang:INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `withErrorLocale` method of `com.opencsv.CSVParserBuilder`.
    #
    # + arg0 - The `javautil:Locale` value required to map with the Java method parameter.
    # + return - The `CSVParserBuilder` value returning from the Java mapping.
    public function withErrorLocale(javautil:Locale arg0) returns CSVParserBuilder {
        handle externalObj = com_opencsv_CSVParserBuilder_withErrorLocale(self.jObj, arg0.jObj);
        CSVParserBuilder newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `withEscapeChar` method of `com.opencsv.CSVParserBuilder`.
    #
    # + arg0 - The `int` value required to map with the Java method parameter.
    # + return - The `CSVParserBuilder` value returning from the Java mapping.
    public function withEscapeChar(int arg0) returns CSVParserBuilder {
        handle externalObj = com_opencsv_CSVParserBuilder_withEscapeChar(self.jObj, arg0);
        CSVParserBuilder newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `withFieldAsNull` method of `com.opencsv.CSVParserBuilder`.
    #
    # + arg0 - The `comopencsvenums:CSVReaderNullFieldIndicator` value required to map with the Java method parameter.
    # + return - The `CSVParserBuilder` value returning from the Java mapping.
    public function withFieldAsNull(comopencsvenums:CSVReaderNullFieldIndicator arg0) returns CSVParserBuilder {
        handle externalObj = com_opencsv_CSVParserBuilder_withFieldAsNull(self.jObj, arg0.jObj);
        CSVParserBuilder newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `withIgnoreLeadingWhiteSpace` method of `com.opencsv.CSVParserBuilder`.
    #
    # + arg0 - The `boolean` value required to map with the Java method parameter.
    # + return - The `CSVParserBuilder` value returning from the Java mapping.
    public function withIgnoreLeadingWhiteSpace(boolean arg0) returns CSVParserBuilder {
        handle externalObj = com_opencsv_CSVParserBuilder_withIgnoreLeadingWhiteSpace(self.jObj, arg0);
        CSVParserBuilder newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `withIgnoreQuotations` method of `com.opencsv.CSVParserBuilder`.
    #
    # + arg0 - The `boolean` value required to map with the Java method parameter.
    # + return - The `CSVParserBuilder` value returning from the Java mapping.
    public function withIgnoreQuotations(boolean arg0) returns CSVParserBuilder {
        handle externalObj = com_opencsv_CSVParserBuilder_withIgnoreQuotations(self.jObj, arg0);
        CSVParserBuilder newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `withQuoteChar` method of `com.opencsv.CSVParserBuilder`.
    #
    # + arg0 - The `int` value required to map with the Java method parameter.
    # + return - The `CSVParserBuilder` value returning from the Java mapping.
    public function withQuoteChar(int arg0) returns CSVParserBuilder {
        handle externalObj = com_opencsv_CSVParserBuilder_withQuoteChar(self.jObj, arg0);
        CSVParserBuilder newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `withSeparator` method of `com.opencsv.CSVParserBuilder`.
    #
    # + arg0 - The `int` value required to map with the Java method parameter.
    # + return - The `CSVParserBuilder` value returning from the Java mapping.
    public function withSeparator(int arg0) returns CSVParserBuilder {
        handle externalObj = com_opencsv_CSVParserBuilder_withSeparator(self.jObj, arg0);
        CSVParserBuilder newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `withStrictQuotes` method of `com.opencsv.CSVParserBuilder`.
    #
    # + arg0 - The `boolean` value required to map with the Java method parameter.
    # + return - The `CSVParserBuilder` value returning from the Java mapping.
    public function withStrictQuotes(boolean arg0) returns CSVParserBuilder {
        handle externalObj = com_opencsv_CSVParserBuilder_withStrictQuotes(self.jObj, arg0);
        CSVParserBuilder newObj = new (externalObj);
        return newObj;
    }

}

# The constructor function to generate an object of `com.opencsv.CSVParserBuilder`.
#
# + return - The new `CSVParserBuilder` class generated.
public function newCSVParserBuilder1() returns CSVParserBuilder {
    handle externalObj = com_opencsv_CSVParserBuilder_newCSVParserBuilder1();
    CSVParserBuilder newObj = new (externalObj);
    return newObj;
}

function com_opencsv_CSVParserBuilder_build(handle receiver) returns handle = @java:Method {
    name: "build",
    'class: "com.opencsv.CSVParserBuilder",
    paramTypes: []
} external;

function com_opencsv_CSVParserBuilder_equals(handle receiver, handle arg0) returns boolean = @java:Method {
    name: "equals",
    'class: "com.opencsv.CSVParserBuilder",
    paramTypes: ["java.lang.Object"]
} external;

function com_opencsv_CSVParserBuilder_getClass(handle receiver) returns handle = @java:Method {
    name: "getClass",
    'class: "com.opencsv.CSVParserBuilder",
    paramTypes: []
} external;

function com_opencsv_CSVParserBuilder_getEscapeChar(handle receiver) returns int = @java:Method {
    name: "getEscapeChar",
    'class: "com.opencsv.CSVParserBuilder",
    paramTypes: []
} external;

function com_opencsv_CSVParserBuilder_getQuoteChar(handle receiver) returns int = @java:Method {
    name: "getQuoteChar",
    'class: "com.opencsv.CSVParserBuilder",
    paramTypes: []
} external;

function com_opencsv_CSVParserBuilder_getSeparator(handle receiver) returns int = @java:Method {
    name: "getSeparator",
    'class: "com.opencsv.CSVParserBuilder",
    paramTypes: []
} external;

function com_opencsv_CSVParserBuilder_hashCode(handle receiver) returns int = @java:Method {
    name: "hashCode",
    'class: "com.opencsv.CSVParserBuilder",
    paramTypes: []
} external;

function com_opencsv_CSVParserBuilder_isIgnoreLeadingWhiteSpace(handle receiver) returns boolean = @java:Method {
    name: "isIgnoreLeadingWhiteSpace",
    'class: "com.opencsv.CSVParserBuilder",
    paramTypes: []
} external;

function com_opencsv_CSVParserBuilder_isIgnoreQuotations(handle receiver) returns boolean = @java:Method {
    name: "isIgnoreQuotations",
    'class: "com.opencsv.CSVParserBuilder",
    paramTypes: []
} external;

function com_opencsv_CSVParserBuilder_isStrictQuotes(handle receiver) returns boolean = @java:Method {
    name: "isStrictQuotes",
    'class: "com.opencsv.CSVParserBuilder",
    paramTypes: []
} external;

function com_opencsv_CSVParserBuilder_notify(handle receiver) = @java:Method {
    name: "notify",
    'class: "com.opencsv.CSVParserBuilder",
    paramTypes: []
} external;

function com_opencsv_CSVParserBuilder_notifyAll(handle receiver) = @java:Method {
    name: "notifyAll",
    'class: "com.opencsv.CSVParserBuilder",
    paramTypes: []
} external;

function com_opencsv_CSVParserBuilder_nullFieldIndicator(handle receiver) returns handle = @java:Method {
    name: "nullFieldIndicator",
    'class: "com.opencsv.CSVParserBuilder",
    paramTypes: []
} external;

function com_opencsv_CSVParserBuilder_wait(handle receiver) returns error? = @java:Method {
    name: "wait",
    'class: "com.opencsv.CSVParserBuilder",
    paramTypes: []
} external;

function com_opencsv_CSVParserBuilder_wait2(handle receiver, int arg0) returns error? = @java:Method {
    name: "wait",
    'class: "com.opencsv.CSVParserBuilder",
    paramTypes: ["long"]
} external;

function com_opencsv_CSVParserBuilder_wait3(handle receiver, int arg0, int arg1) returns error? = @java:Method {
    name: "wait",
    'class: "com.opencsv.CSVParserBuilder",
    paramTypes: ["long", "int"]
} external;

function com_opencsv_CSVParserBuilder_withErrorLocale(handle receiver, handle arg0) returns handle = @java:Method {
    name: "withErrorLocale",
    'class: "com.opencsv.CSVParserBuilder",
    paramTypes: ["java.util.Locale"]
} external;

function com_opencsv_CSVParserBuilder_withEscapeChar(handle receiver, int arg0) returns handle = @java:Method {
    name: "withEscapeChar",
    'class: "com.opencsv.CSVParserBuilder",
    paramTypes: ["char"]
} external;

function com_opencsv_CSVParserBuilder_withFieldAsNull(handle receiver, handle arg0) returns handle = @java:Method {
    name: "withFieldAsNull",
    'class: "com.opencsv.CSVParserBuilder",
    paramTypes: ["com.opencsv.enums.CSVReaderNullFieldIndicator"]
} external;

function com_opencsv_CSVParserBuilder_withIgnoreLeadingWhiteSpace(handle receiver, boolean arg0) returns handle = @java:Method {
    name: "withIgnoreLeadingWhiteSpace",
    'class: "com.opencsv.CSVParserBuilder",
    paramTypes: ["boolean"]
} external;

function com_opencsv_CSVParserBuilder_withIgnoreQuotations(handle receiver, boolean arg0) returns handle = @java:Method {
    name: "withIgnoreQuotations",
    'class: "com.opencsv.CSVParserBuilder",
    paramTypes: ["boolean"]
} external;

function com_opencsv_CSVParserBuilder_withQuoteChar(handle receiver, int arg0) returns handle = @java:Method {
    name: "withQuoteChar",
    'class: "com.opencsv.CSVParserBuilder",
    paramTypes: ["char"]
} external;

function com_opencsv_CSVParserBuilder_withSeparator(handle receiver, int arg0) returns handle = @java:Method {
    name: "withSeparator",
    'class: "com.opencsv.CSVParserBuilder",
    paramTypes: ["char"]
} external;

function com_opencsv_CSVParserBuilder_withStrictQuotes(handle receiver, boolean arg0) returns handle = @java:Method {
    name: "withStrictQuotes",
    'class: "com.opencsv.CSVParserBuilder",
    paramTypes: ["boolean"]
} external;

function com_opencsv_CSVParserBuilder_newCSVParserBuilder1() returns handle = @java:Constructor {
    'class: "com.opencsv.CSVParserBuilder",
    paramTypes: []
} external;

