import csvplus.java.io as javaio;
import ballerina/test;
import csvplus.com.opencsv as csv;

@test:Config
public function canParseACSVFile() returns error? {
    var fr = check javaio:newFileReader4("tests/test-02.csv");
    var csvReader = csv:newCSVReader1(fr);
    string[][] rows = [];
    while true {
        string[]? values = check csvReader.readNext();
        if values is null {
            break;
        }
        rows.push(values);
    }
    test:assertEquals(2, rows.length());
    test:assertEquals("Value A1", rows[0][0]);
    test:assertEquals("Value B2", rows[1][1]);
    test:assertEquals("Value C2", rows[1][2]);
}

