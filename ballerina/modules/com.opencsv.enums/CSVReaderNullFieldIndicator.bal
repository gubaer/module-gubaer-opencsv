import ballerina/jballerina.java;
import ballerina/jballerina.java.arrays as jarrays;
import csvplus.java.lang as javalang;

# Ballerina class mapping for the Java `com.opencsv.enums.CSVReaderNullFieldIndicator` class.
@java:Binding {'class: "com.opencsv.enums.CSVReaderNullFieldIndicator"}
public distinct class CSVReaderNullFieldIndicator {

    *java:JObject;
    *javalang:Enum;

    # The `handle` field that stores the reference to the `com.opencsv.enums.CSVReaderNullFieldIndicator` object.
    public handle jObj;

    # The init function of the Ballerina class mapping the `com.opencsv.enums.CSVReaderNullFieldIndicator` Java class.
    #
    # + obj - The `handle` value containing the Java reference of the object.
    public function init(handle obj) {
        self.jObj = obj;
    }

    # The function to retrieve the string representation of the Ballerina class mapping the `com.opencsv.enums.CSVReaderNullFieldIndicator` Java class.
    #
    # + return - The `string` form of the Java object instance.
    public function toString() returns string {
        return java:toString(self.jObj) ?: "null";
    }
    # The function that maps to the `compareTo` method of `com.opencsv.enums.CSVReaderNullFieldIndicator`.
    #
    # + arg0 - The `javalang:Enum` value required to map with the Java method parameter.
    # + return - The `int` value returning from the Java mapping.
    public function compareTo(javalang:Enum arg0) returns int {
        return com_opencsv_enums_CSVReaderNullFieldIndicator_compareTo(self.jObj, arg0.jObj);
    }

    # The function that maps to the `equals` method of `com.opencsv.enums.CSVReaderNullFieldIndicator`.
    #
    # + arg0 - The `javalang:Object` value required to map with the Java method parameter.
    # + return - The `boolean` value returning from the Java mapping.
    public function 'equals(javalang:Object arg0) returns boolean {
        return com_opencsv_enums_CSVReaderNullFieldIndicator_equals(self.jObj, arg0.jObj);
    }

    # The function that maps to the `getClass` method of `com.opencsv.enums.CSVReaderNullFieldIndicator`.
    #
    # + return - The `javalang:Class` value returning from the Java mapping.
    public function getClass() returns javalang:Class {
        handle externalObj = com_opencsv_enums_CSVReaderNullFieldIndicator_getClass(self.jObj);
        javalang:Class newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getDeclaringClass` method of `com.opencsv.enums.CSVReaderNullFieldIndicator`.
    #
    # + return - The `javalang:Class` value returning from the Java mapping.
    public function getDeclaringClass() returns javalang:Class {
        handle externalObj = com_opencsv_enums_CSVReaderNullFieldIndicator_getDeclaringClass(self.jObj);
        javalang:Class newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `hashCode` method of `com.opencsv.enums.CSVReaderNullFieldIndicator`.
    #
    # + return - The `int` value returning from the Java mapping.
    public function hashCode() returns int {
        return com_opencsv_enums_CSVReaderNullFieldIndicator_hashCode(self.jObj);
    }

    # The function that maps to the `name` method of `com.opencsv.enums.CSVReaderNullFieldIndicator`.
    #
    # + return - The `string` value returning from the Java mapping.
    public function name() returns string? {
        return java:toString(com_opencsv_enums_CSVReaderNullFieldIndicator_name(self.jObj));
    }

    # The function that maps to the `notify` method of `com.opencsv.enums.CSVReaderNullFieldIndicator`.
    public function notify() {
        com_opencsv_enums_CSVReaderNullFieldIndicator_notify(self.jObj);
    }

    # The function that maps to the `notifyAll` method of `com.opencsv.enums.CSVReaderNullFieldIndicator`.
    public function notifyAll() {
        com_opencsv_enums_CSVReaderNullFieldIndicator_notifyAll(self.jObj);
    }

    # The function that maps to the `ordinal` method of `com.opencsv.enums.CSVReaderNullFieldIndicator`.
    #
    # + return - The `int` value returning from the Java mapping.
    public function ordinal() returns int {
        return com_opencsv_enums_CSVReaderNullFieldIndicator_ordinal(self.jObj);
    }

    # The function that maps to the `wait` method of `com.opencsv.enums.CSVReaderNullFieldIndicator`.
    #
    # + return - The `javalang:InterruptedException` value returning from the Java mapping.
    public function 'wait() returns javalang:InterruptedException? {
        error|() externalObj = com_opencsv_enums_CSVReaderNullFieldIndicator_wait(self.jObj);
        if (externalObj is error) {
            javalang:InterruptedException e = error javalang:InterruptedException(javalang:INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `wait` method of `com.opencsv.enums.CSVReaderNullFieldIndicator`.
    #
    # + arg0 - The `int` value required to map with the Java method parameter.
    # + return - The `javalang:InterruptedException` value returning from the Java mapping.
    public function wait2(int arg0) returns javalang:InterruptedException? {
        error|() externalObj = com_opencsv_enums_CSVReaderNullFieldIndicator_wait2(self.jObj, arg0);
        if (externalObj is error) {
            javalang:InterruptedException e = error javalang:InterruptedException(javalang:INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `wait` method of `com.opencsv.enums.CSVReaderNullFieldIndicator`.
    #
    # + arg0 - The `int` value required to map with the Java method parameter.
    # + arg1 - The `int` value required to map with the Java method parameter.
    # + return - The `javalang:InterruptedException` value returning from the Java mapping.
    public function wait3(int arg0, int arg1) returns javalang:InterruptedException? {
        error|() externalObj = com_opencsv_enums_CSVReaderNullFieldIndicator_wait3(self.jObj, arg0, arg1);
        if (externalObj is error) {
            javalang:InterruptedException e = error javalang:InterruptedException(javalang:INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

}

# The function that maps to the `valueOf` method of `com.opencsv.enums.CSVReaderNullFieldIndicator`.
#
# + arg0 - The `javalang:Class` value required to map with the Java method parameter.
# + arg1 - The `string` value required to map with the Java method parameter.
# + return - The `javalang:Enum` value returning from the Java mapping.
public function CSVReaderNullFieldIndicator_valueOf(javalang:Class arg0, string arg1) returns javalang:Enum {
    handle externalObj = com_opencsv_enums_CSVReaderNullFieldIndicator_valueOf(arg0.jObj, java:fromString(arg1));
    javalang:Enum newObj = new (externalObj);
    return newObj;
}

# The function that maps to the `valueOf` method of `com.opencsv.enums.CSVReaderNullFieldIndicator`.
#
# + arg0 - The `string` value required to map with the Java method parameter.
# + return - The `CSVReaderNullFieldIndicator` value returning from the Java mapping.
public function CSVReaderNullFieldIndicator_valueOf2(string arg0) returns CSVReaderNullFieldIndicator {
    handle externalObj = com_opencsv_enums_CSVReaderNullFieldIndicator_valueOf2(java:fromString(arg0));
    CSVReaderNullFieldIndicator newObj = new (externalObj);
    return newObj;
}

# The function that maps to the `values` method of `com.opencsv.enums.CSVReaderNullFieldIndicator`.
#
# + return - The `CSVReaderNullFieldIndicator[]` value returning from the Java mapping.
public function CSVReaderNullFieldIndicator_values() returns CSVReaderNullFieldIndicator[]|error {
    handle externalObj = com_opencsv_enums_CSVReaderNullFieldIndicator_values();
    CSVReaderNullFieldIndicator[] newObj = [];
    handle[] anyObj = <handle[]>check jarrays:fromHandle(externalObj, "handle");
    int count = anyObj.length();
    foreach int i in 0 ... count - 1 {
        CSVReaderNullFieldIndicator element = new (anyObj[i]);
        newObj[i] = element;
    }
    return newObj;
}

# The function that retrieves the value of the public field `EMPTY_SEPARATORS`.
#
# + return - The `CSVReaderNullFieldIndicator` value of the field.
public function CSVReaderNullFieldIndicator_getEMPTY_SEPARATORS() returns CSVReaderNullFieldIndicator {
    handle externalObj = com_opencsv_enums_CSVReaderNullFieldIndicator_getEMPTY_SEPARATORS();
    CSVReaderNullFieldIndicator newObj = new (externalObj);
    return newObj;
}

# The function that retrieves the value of the public field `EMPTY_QUOTES`.
#
# + return - The `CSVReaderNullFieldIndicator` value of the field.
public function CSVReaderNullFieldIndicator_getEMPTY_QUOTES() returns CSVReaderNullFieldIndicator {
    handle externalObj = com_opencsv_enums_CSVReaderNullFieldIndicator_getEMPTY_QUOTES();
    CSVReaderNullFieldIndicator newObj = new (externalObj);
    return newObj;
}

# The function that retrieves the value of the public field `BOTH`.
#
# + return - The `CSVReaderNullFieldIndicator` value of the field.
public function CSVReaderNullFieldIndicator_getBOTH() returns CSVReaderNullFieldIndicator {
    handle externalObj = com_opencsv_enums_CSVReaderNullFieldIndicator_getBOTH();
    CSVReaderNullFieldIndicator newObj = new (externalObj);
    return newObj;
}

# The function that retrieves the value of the public field `NEITHER`.
#
# + return - The `CSVReaderNullFieldIndicator` value of the field.
public function CSVReaderNullFieldIndicator_getNEITHER() returns CSVReaderNullFieldIndicator {
    handle externalObj = com_opencsv_enums_CSVReaderNullFieldIndicator_getNEITHER();
    CSVReaderNullFieldIndicator newObj = new (externalObj);
    return newObj;
}

function com_opencsv_enums_CSVReaderNullFieldIndicator_compareTo(handle receiver, handle arg0) returns int = @java:Method {
    name: "compareTo",
    'class: "com.opencsv.enums.CSVReaderNullFieldIndicator",
    paramTypes: ["java.lang.Enum"]
} external;

function com_opencsv_enums_CSVReaderNullFieldIndicator_equals(handle receiver, handle arg0) returns boolean = @java:Method {
    name: "equals",
    'class: "com.opencsv.enums.CSVReaderNullFieldIndicator",
    paramTypes: ["java.lang.Object"]
} external;

function com_opencsv_enums_CSVReaderNullFieldIndicator_getClass(handle receiver) returns handle = @java:Method {
    name: "getClass",
    'class: "com.opencsv.enums.CSVReaderNullFieldIndicator",
    paramTypes: []
} external;

function com_opencsv_enums_CSVReaderNullFieldIndicator_getDeclaringClass(handle receiver) returns handle = @java:Method {
    name: "getDeclaringClass",
    'class: "com.opencsv.enums.CSVReaderNullFieldIndicator",
    paramTypes: []
} external;

function com_opencsv_enums_CSVReaderNullFieldIndicator_hashCode(handle receiver) returns int = @java:Method {
    name: "hashCode",
    'class: "com.opencsv.enums.CSVReaderNullFieldIndicator",
    paramTypes: []
} external;

function com_opencsv_enums_CSVReaderNullFieldIndicator_name(handle receiver) returns handle = @java:Method {
    name: "name",
    'class: "com.opencsv.enums.CSVReaderNullFieldIndicator",
    paramTypes: []
} external;

function com_opencsv_enums_CSVReaderNullFieldIndicator_notify(handle receiver) = @java:Method {
    name: "notify",
    'class: "com.opencsv.enums.CSVReaderNullFieldIndicator",
    paramTypes: []
} external;

function com_opencsv_enums_CSVReaderNullFieldIndicator_notifyAll(handle receiver) = @java:Method {
    name: "notifyAll",
    'class: "com.opencsv.enums.CSVReaderNullFieldIndicator",
    paramTypes: []
} external;

function com_opencsv_enums_CSVReaderNullFieldIndicator_ordinal(handle receiver) returns int = @java:Method {
    name: "ordinal",
    'class: "com.opencsv.enums.CSVReaderNullFieldIndicator",
    paramTypes: []
} external;

function com_opencsv_enums_CSVReaderNullFieldIndicator_valueOf(handle arg0, handle arg1) returns handle = @java:Method {
    name: "valueOf",
    'class: "com.opencsv.enums.CSVReaderNullFieldIndicator",
    paramTypes: ["java.lang.Class", "java.lang.String"]
} external;

function com_opencsv_enums_CSVReaderNullFieldIndicator_valueOf2(handle arg0) returns handle = @java:Method {
    name: "valueOf",
    'class: "com.opencsv.enums.CSVReaderNullFieldIndicator",
    paramTypes: ["java.lang.String"]
} external;

function com_opencsv_enums_CSVReaderNullFieldIndicator_values() returns handle = @java:Method {
    name: "values",
    'class: "com.opencsv.enums.CSVReaderNullFieldIndicator",
    paramTypes: []
} external;

function com_opencsv_enums_CSVReaderNullFieldIndicator_wait(handle receiver) returns error? = @java:Method {
    name: "wait",
    'class: "com.opencsv.enums.CSVReaderNullFieldIndicator",
    paramTypes: []
} external;

function com_opencsv_enums_CSVReaderNullFieldIndicator_wait2(handle receiver, int arg0) returns error? = @java:Method {
    name: "wait",
    'class: "com.opencsv.enums.CSVReaderNullFieldIndicator",
    paramTypes: ["long"]
} external;

function com_opencsv_enums_CSVReaderNullFieldIndicator_wait3(handle receiver, int arg0, int arg1) returns error? = @java:Method {
    name: "wait",
    'class: "com.opencsv.enums.CSVReaderNullFieldIndicator",
    paramTypes: ["long", "int"]
} external;

function com_opencsv_enums_CSVReaderNullFieldIndicator_getEMPTY_SEPARATORS() returns handle = @java:FieldGet {
    name: "EMPTY_SEPARATORS",
    'class: "com.opencsv.enums.CSVReaderNullFieldIndicator"
} external;

function com_opencsv_enums_CSVReaderNullFieldIndicator_getEMPTY_QUOTES() returns handle = @java:FieldGet {
    name: "EMPTY_QUOTES",
    'class: "com.opencsv.enums.CSVReaderNullFieldIndicator"
} external;

function com_opencsv_enums_CSVReaderNullFieldIndicator_getBOTH() returns handle = @java:FieldGet {
    name: "BOTH",
    'class: "com.opencsv.enums.CSVReaderNullFieldIndicator"
} external;

function com_opencsv_enums_CSVReaderNullFieldIndicator_getNEITHER() returns handle = @java:FieldGet {
    name: "NEITHER",
    'class: "com.opencsv.enums.CSVReaderNullFieldIndicator"
} external;

