# clears the local ballerina repo. Use it to resolve caching
# issues with the local ballerina repo
# see https://github.com/ballerina-platform/ballerina-lang/issues/32153
reset-balerina-local-repo:
	rm -rf $(HOME)/.ballerina/repositories/local
